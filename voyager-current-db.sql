-- MySQL dump 10.15  Distrib 10.0.34-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: 2018_ultramimi
-- ------------------------------------------------------
-- Server version	10.0.34-MariaDB-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `about_post`
--

DROP TABLE IF EXISTS `about_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `about_post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about_post`
--

LOCK TABLES `about_post` WRITE;
/*!40000 ALTER TABLE `about_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `about_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image_desktop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (3,'Tips Memilih Mainan Edukasi Untuk Anak','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua','banner/June2018/jJYpdQb7ptEKelCf5VaO.jpg','banner/June2018/P21DOJmeDKgeG859fc78.jpg','HOME',1,1,'2018-06-24 23:15:03','2018-06-24 23:15:03'),(4,'Tumbuh Alami, Alami Tumbuh','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua','banner/June2018/FAXSQesPR5yiT9pPt6Bj.jpg','banner/June2018/bImYew1I6UtFPBfzUV9b.jpg','HOME',1,2,'2018-06-24 23:17:00','2018-06-25 01:30:21');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner_cta`
--

DROP TABLE IF EXISTS `banner_cta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner_cta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cta_target` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner_cta`
--

LOCK TABLES `banner_cta` WRITE;
/*!40000 ALTER TABLE `banner_cta` DISABLE KEYS */;
INSERT INTO `banner_cta` VALUES (1,'register_now','Register Now','custom','http://www.tianrosandhy.com',NULL,'2018-06-20 20:39:54','2018-06-24 22:32:28',3),(2,'contact_us','Contact Us','page','contact-us',NULL,'2018-06-20 20:40:14','2018-06-20 20:40:14',3);
/*!40000 ALTER TABLE `banner_cta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner_to_cta`
--

DROP TABLE IF EXISTS `banner_to_cta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner_to_cta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) DEFAULT NULL,
  `banner_cta_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner_to_cta`
--

LOCK TABLES `banner_to_cta` WRITE;
/*!40000 ALTER TABLE `banner_to_cta` DISABLE KEYS */;
INSERT INTO `banner_to_cta` VALUES (3,1,1,'2018-06-20 21:41:49','2018-06-20 21:41:49'),(4,1,2,'2018-06-20 21:41:49','2018-06-20 21:41:49'),(5,3,1,'2018-06-24 23:15:03','2018-06-24 23:15:03'),(7,4,1,'2018-06-25 01:30:21','2018-06-25 01:30:21'),(8,4,3,'2018-06-25 01:30:21','2018-06-25 01:30:21');
/*!40000 ALTER TABLE `banner_to_cta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bread_templates`
--

DROP TABLE IF EXISTS `bread_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bread_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bread_templates_name_unique` (`name`),
  UNIQUE KEY `bread_templates_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bread_templates`
--

LOCK TABLES `bread_templates` WRITE;
/*!40000 ALTER TABLE `bread_templates` DISABLE KEYS */;
INSERT INTO `bread_templates` VALUES (1,'Columns 8/4','columns-8-4','<div class=\"row\">\n    <div class=\"col-sm-8 col-md-8 col-lg-8\">\n        <div class=\"panel panel-body\">@stack(\"r01_lf\")</div>\n    </div>\n    <div class=\"col-sm-4 col-md-4 col-lg-4\">\n        <div class=\"panel panel-body\">@stack(\"r01_rg\")</div>\n    </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-sm-8 col-md-8 col-lg-8\">\n        <div class=\"panel panel-body\">@stack(\"r02_lf\")</div>\n    </div>\n    <div class=\"col-sm-4 col-md-4 col-lg-4\">\n        <div class=\"panel panel-body\">@stack(\"r02_rg\")</div>\n    </div>\n</div>','2018-05-30 21:30:44','2018-05-30 21:30:44'),(2,'Columns 6/6','columns-6-6','<div class=\"row\">\n    <div class=\"col-sm-6 col-md-6 col-lg-6\">\n        <div class=\"panel panel-body\">@stack(\"lf\")</div>\n    </div>\n    <div class=\"col-sm-6 col-md-6 col-lg-6\">\n        <div class=\"panel panel-body\">@stack(\"rg\")</div>\n    </div>\n</div>','2018-05-30 21:30:44','2018-05-30 21:30:44'),(3,'Columns 4/8','columns-4-8','<div class=\"row\">\n    <div class=\"col-sm-4 col-md-4 col-lg-4\">\n        <div class=\"panel panel-body\">@stack(\"r01_rg\")</div>\n    </div>\n    <div class=\"col-sm-8 col-md-8 col-lg-8\">\n        <div class=\"panel panel-body\">@stack(\"r01_lf\")</div>\n    </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-sm-4 col-md-4 col-lg-4\">\n        <div class=\"panel panel-body\">@stack(\"r02_rg\")</div>\n    </div>\n    <div class=\"col-sm-8 col-md-8 col-lg-8\">\n        <div class=\"panel panel-body\">@stack(\"r02_lf\")</div>\n    </div>\n</div>','2018-05-30 21:30:45','2018-05-30 21:30:45');
/*!40000 ALTER TABLE `bread_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (3,NULL,1,'Charenting','charenting','2018-06-24 23:09:59','2018-06-24 23:09:59'),(4,NULL,1,'Artikel','artikel','2018-06-24 23:10:28','2018-06-24 23:10:28'),(5,NULL,1,'Temu Mimi & Dokter','temu-mimi-and-dokter','2018-06-24 23:10:45','2018-06-24 23:10:45');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Name',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Password',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,1,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,0,0,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(12,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(13,2,'id','number','ID',1,0,0,0,0,0,'',1),(14,2,'name','text','Name',1,1,1,1,1,1,'',2),(15,2,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),(16,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),(17,3,'id','number','ID',1,0,0,0,0,0,'',1),(18,3,'name','text','Name',1,1,1,1,1,1,'',2),(19,3,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),(20,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),(21,3,'display_name','text','Display Name',1,1,1,1,1,1,'',5),(22,1,'role_id','text','Role',0,1,1,1,1,1,NULL,9),(23,4,'id','number','ID',1,0,0,0,0,0,NULL,1),(24,4,'parent_id','select_dropdown','Parent',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(25,4,'order','text','Order',1,1,1,1,1,1,'{\"default\":1}',3),(26,4,'name','text','Name',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',4),(27,4,'slug','text','Slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(28,4,'created_at','timestamp','Created At',0,0,1,0,0,0,NULL,6),(29,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(30,5,'id','number','ID',1,0,0,0,0,0,NULL,1),(31,5,'author_id','text','Author',1,0,0,1,0,1,NULL,2),(32,5,'category_id','text','Category',0,0,1,1,1,0,NULL,3),(33,5,'title','text','Title',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',4),(34,5,'excerpt','text_area','Excerpt',0,0,1,1,1,1,NULL,5),(35,5,'body','rich_text_box','Body',1,0,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',6),(36,5,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1200\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(37,5,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}',8),(38,5,'meta_description','text_area','Meta Description',0,0,1,1,1,1,NULL,10),(39,5,'meta_keywords','text_area','Meta Keywords',0,0,1,1,1,1,NULL,11),(40,5,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"PUBLISHED\",\"DRAFT\":\"DRAFT\",\"PENDING\":\"PENDING\"}}',12),(41,5,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,14),(42,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,15),(43,5,'seo_title','text','SEO Title',0,0,1,1,1,1,NULL,16),(44,5,'featured','number','Home Featured',1,1,1,1,1,1,'{\"default\":\"0\"}',18),(45,6,'id','number','ID',1,0,0,0,0,0,NULL,1),(46,6,'author_id','text','Author',0,0,0,0,0,0,NULL,3),(47,6,'title','text','Title',1,1,1,1,1,1,'{\"template\":{\"slug\":\"columns-8-4\",\"stack\":\"r01_lf\"},\"validation\":{\"rule\":\"required\"}}',4),(48,6,'excerpt','text_area','Excerpt',0,0,1,1,1,1,'{\"template\":{\"slug\":\"columns-8-4\",\"stack\":\"r02_lf\"}}',8),(49,6,'body','rich_text_box','Body',0,0,1,1,1,1,'{\"template\":{\"slug\":\"columns-8-4\"}}',12),(50,6,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"},\"template\":{\"slug\":\"columns-8-4\",\"stack\":\"r01_lf\"}}',5),(51,6,'meta_description','text','Meta Description',0,0,1,1,1,1,'{\"template\":{\"slug\":\"columns-8-4\",\"stack\":\"r01_lf\"}}',6),(52,6,'meta_keywords','text','Meta Keywords',0,0,1,1,1,1,'{\"template\":{\"slug\":\"columns-8-4\",\"stack\":\"r01_lf\"}}',7),(53,6,'status','select_dropdown','Status',0,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"},\"template\":{\"slug\":\"columns-8-4\",\"stack\":\"r01_rg\"}}',9),(54,6,'created_at','timestamp','Created At',0,1,1,0,0,0,'{\"template\":{\"slug\":\"columns-8-4\",\"stack\":\"r01_rg\"}}',10),(55,6,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,13),(56,6,'image','image','Page Image',0,1,1,1,1,1,'{\"template\":{\"slug\":\"columns-8-4\",\"stack\":\"r02_rg\"}}',11),(57,8,'id','text','Id',1,0,0,0,0,0,NULL,1),(58,8,'title','text','Title',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(59,8,'slug','text','Slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}',3),(60,8,'description','rich_text_box','Description',0,0,1,1,1,1,NULL,4),(61,8,'category','text','Category',1,0,0,0,0,0,'{\"validation\":{\"rule\":\"required\"}}',5),(62,8,'stat','select_dropdown','Stat',0,1,1,1,1,1,'{\"default\":\"0\",\"options\":{\"0\":\"Draft\",\"1\":\"Live\"}}',8),(63,8,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,9),(64,8,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,10),(65,8,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,NULL,11),(66,9,'id','text','Id',1,0,0,0,0,0,NULL,1),(67,9,'category_name','text','Category Name',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(68,9,'slug','text','Slug',0,1,1,1,1,1,'{\"slugify\":{\"origin\":\"category_name\",\"forceUpdate\":true}}',3),(69,9,'description','text_area','Description',0,1,1,1,1,1,NULL,4),(70,9,'created_at','timestamp','Created At',0,1,1,1,0,0,NULL,5),(71,9,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(72,9,'stat','select_dropdown','Stat',0,1,1,1,1,1,'{\"default\":\"0\",\"options\":{\"0\":\"Draft\",\"1\":\"Live\"}}',7),(74,8,'image','multiple_images','Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":null},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(75,10,'id','number','id',1,0,0,0,0,0,'',1),(76,10,'name','text','Name',1,1,1,1,1,1,'',2),(77,10,'slug','text','slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',3),(78,10,'view','code_editor','body',1,0,1,1,1,1,'',4),(79,10,'created_at','timestamp','created_at',1,1,1,0,0,0,'',5),(80,10,'updated_at','timestamp','updated_at',1,0,0,0,0,0,'',6),(81,11,'id','text','Id',1,0,0,0,0,0,NULL,1),(82,11,'user_id','text','User Id',1,0,1,1,1,1,NULL,2),(83,11,'media_id','text','Media Id',1,0,1,1,1,1,NULL,3),(84,11,'shortcode','text','Shortcode',0,0,1,1,1,1,NULL,4),(85,11,'username','text','Username',0,0,1,1,1,1,NULL,5),(86,11,'username_thumb','image','Username Thumb',0,0,1,1,1,1,NULL,6),(87,11,'media_type','select_dropdown','Media Type',0,1,1,1,1,1,'{\"default\":\"image\",\"options\":{\"image\":\"Image\",\"video\":\"Video\"}}',7),(88,11,'property','text','Property',0,1,1,1,1,1,NULL,8),(89,11,'media_filepath','image','Media Filepath',0,1,1,1,1,1,NULL,9),(90,11,'media_link','text','Media Link',0,0,1,1,1,1,NULL,10),(91,11,'created_at','text','Created At',0,1,1,1,1,1,NULL,12),(92,11,'is_active','select_dropdown','Is Active',0,1,1,1,1,1,'{\"default\":\"live\",\"options\":{\"0\":\"Draft\",\"1\":\"Live\"}}',13),(93,11,'content','text','Content',0,1,1,1,1,1,NULL,11),(95,5,'post_hasmany_post_related_relationship','relationship','post_related',0,1,0,1,1,1,'{\"model\":\"Module\\\\Post\\\\Models\\\\PostRelatedModel\",\"table\":\"post_related\",\"type\":\"hasMany\",\"column\":\"post_source\",\"key\":\"id\",\"label\":\"post_related_id\",\"pivot_table\":\"bread_templates\",\"pivot\":\"0\",\"taggable\":\"0\"}',17),(96,5,'post_hasone_category_relationship','relationship','categories',0,1,1,1,1,1,'{\"model\":\"Module\\\\Post\\\\Models\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bread_templates\",\"pivot\":\"0\",\"taggable\":\"0\"}',9),(97,12,'id','text','Id',1,0,0,0,0,0,NULL,1),(98,12,'title','text','Title',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(99,12,'description','text_area','Description',0,1,1,1,1,1,NULL,3),(100,12,'image','image','Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"800\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"300\"}}]}',4),(101,12,'order','number','Order',0,1,1,1,1,1,NULL,5),(102,12,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,6),(103,12,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(104,14,'id','text','Id',1,0,0,0,0,0,NULL,1),(105,14,'title','text','Title',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(106,14,'description','text','Description',0,1,1,1,1,1,NULL,3),(107,14,'image_desktop','image','Image Desktop',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1500\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"}]}',4),(108,14,'image_mobile','image','Image Mobile',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1500\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"}]}',5),(109,14,'location','select_dropdown','Location',0,1,1,1,1,1,'{\"default\":\"DEFAULT\",\"options\":{\"DEFAULT\":\"Default\",\"HOME\":\"Home\",\"ABOUT\":\"About\",\"KNOWLEDGE\":\"Knowledge\",\"EVENTS\":\"Events\",\"ACTIVITIES\":\"Activities\"}}',6),(110,14,'is_active','select_dropdown','Is Active',0,1,1,1,1,1,'{\"default\":\"0\",\"options\":{\"0\":\"Draft\",\"1\":\"Active\"}}',7),(111,14,'order','number','Order',0,1,1,1,1,1,NULL,8),(112,14,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,9),(113,14,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,10),(114,15,'id','text','Id',1,0,0,0,0,0,NULL,1),(115,15,'title','text','Title',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(116,15,'label','text','Label',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',3),(117,15,'type','select_dropdown','Type',0,1,1,1,1,1,'{\"default\":\"page\",\"options\":{\"page\":\"Pages\",\"popup\":\"Popup\",\"post\":\"Post\",\"custom\":\"Custom URL\"}}',4),(118,15,'url','text','Url',0,1,1,1,1,1,NULL,5),(119,15,'attribute','text','Attribute',0,0,1,1,1,1,NULL,6),(120,15,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,7),(121,15,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,8),(123,14,'banner_hasmany_banner_to_ctum_relationship','relationship','banner_to_cta',0,1,1,1,1,1,'{\"model\":\"Module\\\\Banner\\\\Models\\\\BannerToCTA\",\"table\":\"banner_to_cta\",\"type\":\"hasMany\",\"column\":\"banner_id\",\"key\":\"id\",\"label\":\"banner_cta_id\",\"pivot_table\":\"about_post\",\"pivot\":\"0\",\"taggable\":\"0\"}',11),(125,17,'id','text','Id',1,0,0,0,0,0,NULL,1),(126,17,'title','text','Title',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(127,17,'location','text','Location',0,1,1,1,1,1,NULL,3),(128,17,'date_start','date','Date Start',0,1,1,1,1,1,NULL,4),(129,17,'date_end','date','Date End',0,1,1,1,1,1,NULL,6),(130,17,'excerpt','text_area','Excerpt',0,1,1,1,1,1,NULL,8),(131,17,'content','rich_text_box','Content',0,0,1,1,1,1,NULL,9),(132,17,'image','image','Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1200\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"}]}',10),(133,17,'is_active','select_dropdown','Is Active',0,1,1,1,1,1,'{\"default\":\"0\",\"options\":{\"0\":\"Draft\",\"1\":\"Active\"}}',14),(134,17,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,11),(135,17,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,12),(136,17,'time_start','time','Time Start',0,1,1,1,1,1,NULL,5),(137,17,'time_end','time','Time End',0,1,1,1,1,1,NULL,7),(138,19,'id','text','Id',1,0,0,0,0,0,NULL,1),(139,19,'title','text','Title',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(140,19,'content','text_area','Content',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',3),(141,19,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,4),(142,19,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,5),(143,19,'is_active','select_dropdown','Is Active',0,1,1,1,1,1,'{\"default\":\"0\",\"options\":{\"0\":\"Draft\",\"1\":\"Live\"}}',6),(144,20,'id','text','Id',1,0,0,0,0,0,NULL,1),(145,20,'username','text','Username',0,1,1,1,1,1,NULL,2),(146,20,'email','hidden','Email',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required|unique:member,email\"}}',3),(147,20,'email_inputted','text','Email Inputted',0,1,1,1,1,1,NULL,4),(148,20,'password','password','Password',1,0,0,1,1,1,NULL,5),(149,20,'remember_token','text','Remember Token',0,0,0,0,0,1,NULL,6),(150,20,'hash','text','Hash',0,0,0,0,0,1,NULL,7),(151,20,'is_active','select_dropdown','Is Active',0,1,1,1,1,1,'{\"default\":\"0\",\"options\":{\"0\":\"Not Validated\",\"1\":\"Validated\",\"2\":\"Blocked\"}}',8),(152,20,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,9),(153,20,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,10),(154,5,'most_viewed','number','Most Viewed',0,1,1,1,1,1,'{\"default\":\"0\"}',19),(155,5,'newest','number','Newest',0,1,1,1,1,1,'{\"default\":\"0\"}',20),(156,5,'click','number','Click',0,1,1,1,1,1,'{\"default\":\"0\"}',21),(157,5,'tags','select_multiple','Tags',0,0,1,1,1,1,'{\"id\":\"form-tags\"}',13),(158,17,'event_hasmany_event_subscribe_relationship','relationship','event_subscribe',0,1,1,1,1,1,'{\"model\":\"Module\\\\Event\\\\Models\\\\EventSubscribe\",\"table\":\"event_subscribe\",\"type\":\"hasMany\",\"column\":\"event_id\",\"key\":\"id\",\"label\":\"member_id\",\"pivot_table\":\"about_post\",\"pivot\":\"0\",\"taggable\":\"0\"}',13),(159,22,'id','text','Id',1,0,0,0,0,0,NULL,1),(160,22,'posttype','number','Posttype',1,0,0,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(161,22,'param','text','Param',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',3),(162,22,'content','text_area','Content',0,1,1,1,1,1,NULL,6),(163,22,'content_translated','text_area','Content Translated',0,0,1,1,1,1,NULL,7),(164,22,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,8),(165,22,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,9),(166,23,'id','text','Id',1,0,0,0,0,0,NULL,1),(167,23,'title','text','Title',0,1,1,1,1,1,NULL,2),(168,23,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,4),(169,23,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,5),(170,22,'static_content_hasmany_static_posttype_relationship','relationship','Post Type',0,1,1,1,1,1,'{\"model\":\"Module\\\\StaticContent\\\\Models\\\\StaticPosttype\",\"table\":\"static_posttype\",\"type\":\"hasMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"about_post\",\"pivot\":\"0\",\"taggable\":\"0\"}',4),(171,23,'slug','text','Slug',0,1,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}',3),(172,22,'type','text','Type',0,1,1,1,1,1,NULL,5),(173,6,'pagetype','select_dropdown','Pagetype',0,1,1,1,1,1,'{\"default\":\"normal\",\"options\":{\"normal\":\"Normal Page\",\"popup\":\"Popup Page\"}}',2),(174,15,'cta_target','text','Cta Target',0,0,0,1,1,1,NULL,9),(175,6,'page_hasone_banner_ctum_relationship','relationship','cta',0,1,1,1,1,1,'{\"model\":\"Module\\\\Banner\\\\Models\\\\BannerCTA\",\"table\":\"banner_cta\",\"type\":\"hasOne\",\"column\":\"cta_target\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"about_post\",\"pivot\":\"0\",\"taggable\":\"0\"}',14),(176,6,'cta_target','text','Cta Target',0,0,0,1,1,1,NULL,14);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy',NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-29 23:27:05','2018-06-04 23:12:52'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2018-05-29 23:27:05','2018-05-29 23:27:05'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2018-05-29 23:27:05','2018-05-29 23:27:05'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-29 23:28:11','2018-06-24 23:44:55'),(5,'posts','post','Post','Posts','voyager-news','Module\\Post\\Models\\PostModel','TCG\\Voyager\\Policies\\PostPolicy','\\Module\\Post\\Controllers\\PostController',NULL,1,1,'{\"order_column\":null,\"order_display_column\":null}','2018-05-29 23:28:12','2018-06-24 23:32:19'),(6,'pages','pages','Page','Pages','voyager-file-text','Module\\Page\\Models\\PageModel',NULL,'\\Module\\Page\\Controllers\\PageController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-29 23:28:13','2018-06-25 02:29:25'),(8,'product','product','Product','Products','voyager-window-list','Module\\Product\\Models\\Product',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null}','2018-05-30 20:35:33','2018-06-20 19:10:05'),(9,'product_category','product-category','Product Category','Product Categories','voyager-categories','Module\\Product\\Models\\ProductCategory',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null}','2018-05-30 20:36:20','2018-06-07 22:32:34'),(10,'bread_templates','templates','Template','Templates','voyager-news','Launcher\\BreadTemplates\\Models\\Template',NULL,'','',1,0,NULL,'2018-05-30 21:30:45','2018-05-30 21:30:45'),(11,'instagram','instagram','Instagram','Instagrams',NULL,'Module\\Instagram\\Models\\Instagram',NULL,'\\Module\\Instagram\\Controllers\\InstagramController',NULL,1,0,'{\"order_column\":\"created_at\",\"order_display_column\":null}','2018-06-07 22:51:10','2018-06-08 01:29:01'),(12,'about_post','keunggulan-mimi','About Post','Keunggulan Mimi','voyager-thumbs-up','Module\\About\\Models\\KeunggulanMimi',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-06-20 19:23:05','2018-06-24 23:44:20'),(14,'banner','banner','Banner','Banners',NULL,'Module\\Banner\\Models\\Banner',NULL,'\\Module\\Banner\\Controllers\\BannerController',NULL,1,0,'{\"order_column\":\"order\",\"order_display_column\":\"order\"}','2018-06-20 20:25:51','2018-06-24 23:44:11'),(15,'banner_cta','banner-cta','Click To Action','Click To Actions',NULL,'Module\\Banner\\Models\\BannerCTA',NULL,'\\Module\\Banner\\Controllers\\BannerCTAController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-06-20 20:29:53','2018-06-24 23:44:35'),(17,'events','events','Event','Events','voyager-calendar','Module\\Event\\Models\\Events',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-06-20 22:23:26','2018-06-24 23:45:10'),(19,'faq','faq','FAQ','FAQs','voyager-question','Module\\Faq\\Models\\Faq',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-06-20 22:35:06','2018-06-24 23:45:23'),(20,'member','member','Member','Members','voyager-smile','Module\\Member\\Models\\Member',NULL,'\\Module\\Member\\Controllers\\MemberController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-06-20 23:51:25','2018-06-25 02:02:50'),(22,'static_content','static_content','Static Content','Static Contents',NULL,'Module\\StaticContent\\Models\\StaticContent',NULL,'\\Module\\StaticContent\\Controllers\\StaticContentController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-06-21 19:01:31','2018-06-24 23:46:40'),(23,'static_posttype','static-posttype','Static Posttype','Static Posttypes',NULL,'Module\\StaticContent\\Models\\StaticPosttype',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-06-21 19:35:43','2018-06-21 19:46:13');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_subscribe`
--

DROP TABLE IF EXISTS `event_subscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_subscribe` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_subscribe`
--

LOCK TABLES `event_subscribe` WRITE;
/*!40000 ALTER TABLE `event_subscribe` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_subscribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(254) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `excerpt` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Memasak Jagung Bersama','Denpasar','2018-06-22','2018-06-22','lorem ipsum dolor sit amet','jklasdhkjl askjs jkl sad asdjklhasjkldhaklj adssd adsa sad dsa jklasdhkjl askjs jkl sad asdjklhasjkldhaklj adssd adsa sad dsa jklasdhkjl askjs jkl sad asdjklhasjkldhaklj adssd adsa sad dsa jklasdhkjl askjs jkl sad asdjklhasjkldhaklj adssd adsa sad dsa jklasdhkjl askjs jkl sad asdjklhasjkldhaklj adssd adsa sad dsa jklasdhkjl askjs jkl sad asdjklhasjkldhaklj adssd adsa sad dsa','events/June2018/C9TVC4l6JKQnoXJ6Yuka.jpg',1,'2018-06-20 22:26:40','2018-06-20 22:26:40','01:15:00','05:05:00');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instagram`
--

DROP TABLE IF EXISTS `instagram`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instagram` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `media_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shortcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username_thumb` text COLLATE utf8_unicode_ci,
  `media_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `property` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_filepath` text COLLATE utf8_unicode_ci,
  `media_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instagram`
--

LOCK TABLES `instagram` WRITE;
/*!40000 ALTER TABLE `instagram` DISABLE KEYS */;
INSERT INTO `instagram` VALUES (47,'1563343803','1796928176740470582','Bjv-Kf9gY82','','','image','','instagram/33376569_250413252172809_1843977761098563584_n.jpg','https://www.instagram.com/p/Bjv-Kf9gY82','2018-06-08 04:02:33',0,'Menurut teman-teman, apa itu \'Gereja\'?\nApakah gereja itu, tempat berkumpulnya orang-orang suci? \nAtau mungkin, ada pengertian-pengertian lain menurut teman-teman?\nGereja, bukanlah tempat orang-orang suci berkumpul.\nMelainkan, tempat untuk orang-orang berdosa(dan tak sempurna) yang mendapatkan anugerah dari Allah. Sehingga bisa dikatakan  gereja itu tempat berkumpulnya orang-orang(manusia) \"sakit\".\nNah, di KPPZ kali ini, kita akan belajar lagi lebih dalam mengenai hal ini.\nHari Jumat, 8 Juni 2018\nPukup 19.30 WITA\nDi Aula SD/SMA Zion\n\nSee youu tonight \n\n#dsh #deeperstrongerhigher #kppz #kppzgkka #gkka #gkkai #gkkaiup'),(48,'1588929877','1796908817158683171','Bjv5wx8groj','','','image','','instagram/33441934_2354041484824585_7347345066584702976_n.jpg','https://www.instagram.com/p/Bjv5wx8groj','2018-06-08 03:24:05',0,'Infinity Team \n#kppz #sportday #infinityteam #gkka'),(49,'2091522719','1795756741498978870','Bjrzz34D0I2','','','image','','instagram/34033947_421504328317415_2796461972453851136_n.jpg','https://www.instagram.com/p/Bjrzz34D0I2','2018-06-06 13:15:07',0,'Online Family\nPdt. Daniel Ronda\nGalatia 5:16-26\n\nDigital era dengan kehadiran internet sangat mengubah dunia. Era digital adalah pemberian dan anugrah dari Allah, keindahan yang Tuhan buat untuk pelayanan, kebaikan dan kesejahteraan manusia. Namun digital era tidak hanya mempunyai sisi baik, tetapi juga sisi buruk. Identitas, kerohanian bahkan aktivitas kepribadian sangat dipengaruhi oleh penggunaan gadget.\nKeuntungan internet mempermudah banyak hal dimulai dari pemenuhan kebutuhan, perbankan, pembelajaran, bahkan pelayanan.\nTetapi sisi negatif dari internet ialah informasi negatif tidak lagi dapat disaring, internet juga menciptakan berbagai budaya yang adiktif dan negatif, dan mendorong berbagai kejahatan.\nCara kita mengatasi hal ini ialah:\n1. Hidup oleh Roh (Gal. 5:16), dipimpin oleh Roh (Gal. 5:18) dan hasilkanlah buah Roh (Gal. 5:22).\nHidup yang berdoa dan dipimpin oleh Roh sangat penting agar kita mendapat kekuatan untuk menghadapi cobaan.\n2. Menyalibkan keinginan daging, melatih self-control.\nSalah satu cara menyalibkan keinginan daging ialah transparansi dengan orang terdekat kita.\n3. Bertanggung jawab atas generasi muda.\nSebagai orang tua, tanggung jawab atas anak dalam hal penggunaan adalah hal yang penting. Pemberian edukasi mengenai penggunaan teknologi yang baik pada generasi muda sangat penting. Orang tua harus belajar berbaur ke dalam dunia teknologi agar dapat menjangkau dan mengedukasi generasi muda.\n#LCSMessage #LifeCommunityService #GKKA #Church #Makassar #iLife'),(50,'1563343803','1794207771315535215','BjmTnY-gf1v','','','image','','instagram/33036373_366411093851393_3987287443916193792_n.jpg','https://www.instagram.com/p/BjmTnY-gf1v','2018-06-04 09:57:35',0,'Think Thank \n31-05-2018\n.\n.\n#thinkthankkppz #dsh #deeperstrongerhigher #kppz #kppzgkka #gkka #gkkai #gkkaiup'),(51,'3189740415','1793592692455173119','BjkHw0Jhuv_','','','image','','instagram/32971924_592288261155053_7680479802557464576_n.jpg','https://www.instagram.com/p/BjkHw0Jhuv_','2018-06-03 13:35:32',0,'Thx to my friend from gkka untuk doa dan restunya. #gkka#surabaya#wedding#H&M'),(52,'1563343803','1793574642821165933','BjkDqKHgPNt','','','image','','instagram/32824388_1086576418149350_498769982239277056_n.jpg','https://www.instagram.com/p/BjkDqKHgPNt','2018-06-03 12:59:40',0,'-HUT KPPZ 63-\nFollow HIS Step\n.\n#latepost\n#hutkppz #followhisstep #dsh #deeperstrongerhigher #kppz #kppzgkka #gkka #gkkai #gkkaiup'),(53,'829299386','1793352326758276799','BjjRHCJjB6_','','','image','','instagram/33104989_678406625826438_5341591899083898880_n.jpg','https://www.instagram.com/p/BjjRHCJjB6_','2018-06-03 05:37:58',0,'Stand firm in God\'s behalf~ Let Ur joy live in us..  #sunday #school #praiseandworship #gkka'),(54,'1563343803','1792589134205008871','BjgjlHsg7vn','','','video','','instagram/33116744_2185150398382162_1706938196635418624_n.jpg','https://www.instagram.com/p/BjgjlHsg7vn','2018-06-02 04:29:51',0,'-MYCYN After Movie-\nBuat teman\" yang belum sempat join MYCYN KPPZ atau yang masih mau nonton after movienya lagi, bisa click link di bio yahh \n.\nVideo by : @abedthendean\n#mycynkppz #mycyn2018 #dsh #deeperstrongerhigher #kppz #kppzgkka #gkka #gkkai #gkkaiup'),(55,'2091522719','1792173076529456969','BjfE-ryjqdJ','','','image','','instagram/32673234_200450377255021_6712352388302241792_n.jpg','https://www.instagram.com/p/BjfE-ryjqdJ','2018-06-01 14:35:00',0,'Tidak ada yang lebih tulus menyayangi kita selain keluarga, namun apa yang terjadi bila sesama anggota keluarga saja tidak saling mengasihi dan peduli? \nHadirilah ibadah LCS hari Minggu, 3 Juni 2018 pukul 10.30 di aula SD & SMA Zion! See you guys\n#LCSinvitation #LifeCommunityService #LCS #GKKA #Church #Makassar #iLife'),(56,'1563343803','1792021875703237533','BjeimbDBUud','','','image','','instagram/33425946_2015845745153156_7800624241283432448_n.jpg','https://www.instagram.com/p/BjeimbDBUud','2018-06-01 09:34:36',0,'Seringkali dalam menjalani kehidupan, kita merasa bingung. Bingung kenapa? Bingung bagaimana kita bisa memutuskan sesuatu. Loh kok bingung?  Kan kita tinggal ngikutin pimpinan Roh Kudus. Tapi masalahnya, gimana caranya bedain, yang mana keputusan yang sesuai dengan pimpinan Roh Kudus, yang mana yang bukan? \nWah teman-teman juga pasti pada bingung kan? Gimana cara bedain nya?\nJangan sampai, kita memutuskan sesuatu yang kita pikir berdasarkan pimpinan Roh Kudus, tapi ternyata bukaan \nDi KPPZ kali ini, kita akan belajar memahami hal tersebut. Dan juga kita bisa mengetahui, apakah Roh Kudus itu tinggal di dalam kita atau tidak? Nantikan jawabannya besok \nHari Jumat, 1 Juni 2018\nPukul 19.30 WITA\nDi Ruang Pangestu, STT Jaffray\n\nSee you tonight \n#dsh #deeperstrongerhigher #kppz #kppzgkka #gkka #gkkai #gkkaiup'),(57,'1588929877','1791291349144963899','Bjb8f3LBAs7','','','image','','instagram/33136086_183945452265448_8979285708233506816_n.jpg','https://www.instagram.com/p/Bjb8f3LBAs7','2018-05-31 09:23:10',0,'KPPZ Sport Day 2018 : Infinity Team \n#sportday #sportdaykppz #infinityteam #kppz #kppzgkka #gkka #gkkai #gkkaiup'),(58,'1563343803','1791129633938069310','BjbXumJjdc-','','','image','','instagram/34012571_1736835183074962_558519939468623872_n.jpg','https://www.instagram.com/p/BjbXumJjdc-','2018-05-31 04:01:52',0,'Pasti kita sudah tidak asing lagi dengan istilah \'Bahasa Roh\' . \nBiasanya kita dengar dalam lingkungan kita di gereja.\nNamun seiring berjalannya waktu, kita juga menemukan beragam pemahaman dan perbedaan dalam memahami bahasa Roh. \nApakah makna sesungguhnya dari Bahasa Roh itu. .? Sejauh mana Bahasa Roh itu berperan dalam spiritualitas rohani kita ?\n\nDan bagaimana Bahasa Roh mempengaruhi kehidupan kita sebagai \'orang Kristen\' ?\n\nTemukan jawabannya dalam think thank kali ini \nHari Kamis, 31 Mei 2018\nPukul 19.30 WITA\nDi Pastori GKKA\n.\n#thinkthankkppz #dsh #deeperstrongerhigher #kppz #kppzgkka #gkka #gkkai #gkkaiup');
/*!40000 ALTER TABLE `instagram` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_inputted` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (7,'tianrosandhy','tianrosandhy@gmail.com','tian.ro.san.dhy@gmail.com','$2y$10$VbDuGzGa5d43qNrPSRO3BO1VQ47XKCYaHb2rhXn38ZKY8Fwu.eqTS',NULL,NULL,0,'2018-06-25 01:41:00','2018-06-25 02:07:06');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_meta`
--

DROP TABLE IF EXISTS `member_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_meta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `meta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_meta`
--

LOCK TABLES `member_meta` WRITE;
/*!40000 ALTER TABLE `member_meta` DISABLE KEYS */;
INSERT INTO `member_meta` VALUES (24,7,'fullname','Christian Rosandhy','2018-06-25 02:07:07','2018-06-25 02:07:07'),(25,7,'gender','L','2018-06-25 02:07:07','2018-06-25 02:07:07'),(26,7,'date_of_birth','12/12/1995','2018-06-25 02:07:07','2018-06-25 02:07:07'),(27,7,'address','Jl Tukad Pakerisan no 98/2','2018-06-25 02:07:07','2018-06-25 02:07:07'),(28,7,'userphoto','[\"member\\/June2018\\/3cdcd55665380cf69dcb.jpg\"]','2018-06-25 02:07:07','2018-06-25 02:07:07');
/*!40000 ALTER TABLE `member_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2018-05-29 23:27:06','2018-05-29 23:27:06','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,11,'2018-05-29 23:27:06','2018-06-24 20:32:12','voyager.media.index',NULL),(3,1,'Users','','_self',NULL,'#000000',30,1,'2018-05-29 23:27:07','2018-06-20 22:36:37','voyager.users.index','null'),(4,1,'Roles','','_self',NULL,'#000000',30,2,'2018-05-29 23:27:07','2018-06-20 22:36:46','voyager.roles.index','null'),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,13,'2018-05-29 23:27:07','2018-06-24 20:32:12',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,1,'2018-05-29 23:27:07','2018-06-21 23:55:20','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,2,'2018-05-29 23:27:07','2018-06-21 23:55:20','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,3,'2018-05-29 23:27:07','2018-06-21 23:55:20','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,4,'2018-05-29 23:27:07','2018-06-21 23:55:20','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,12,'2018-05-29 23:27:07','2018-06-24 20:32:12','voyager.settings.index',NULL),(12,1,'Categories','','_self',NULL,'#000000',13,2,'2018-05-29 23:28:11','2018-06-20 19:26:00','voyager.categories.index','null'),(13,1,'Articles','','_self','voyager-news','#000000',NULL,2,'2018-05-29 23:28:13','2018-06-20 19:26:15','voyager.posts.index','null'),(14,1,'Normal Pages','','_self',NULL,'#000000',38,1,'2018-05-29 23:28:14','2018-06-24 20:51:23','voyager.pages.posttype','{\r\n\"type\" : \"normal\"\r\n}'),(16,1,'Product','','_self',NULL,'#000000',23,1,'2018-05-30 20:36:21','2018-06-20 19:25:33','voyager.product.index','null'),(17,1,'Templates','/p4n3lb04rd/templates','_self','voyager-megaphone','#000000',5,5,'2018-05-30 21:30:45','2018-06-21 23:55:20',NULL,''),(18,2,'Dashboard','','_self','voyager-boat','#f52525',NULL,99,'2018-05-30 22:20:49','2018-05-30 22:20:49','voyager.dashboard',NULL),(19,2,'Posts','','_self','voyager-news','#000000',NULL,100,'2018-05-30 22:21:27','2018-05-30 22:21:27','voyager.posts.index',NULL),(22,1,'Keunggulan Mimi','','_self',NULL,'#000000',23,2,'2018-06-20 19:23:05','2018-06-20 19:25:40','voyager.keunggulan-mimi.index','null'),(23,1,'About Us','','_self','voyager-categories','#000000',NULL,3,'2018-06-20 19:23:50','2018-06-20 19:26:26',NULL,''),(24,1,'All Article','','_self',NULL,'#000000',13,1,'2018-06-20 19:24:44','2018-06-21 00:42:05','voyager.post.index','null'),(25,1,'Banner List','','_self',NULL,'#000000',27,1,'2018-06-20 20:25:51','2018-06-20 20:33:47','voyager.banner.index','null'),(26,1,'Click To Actions','','_self',NULL,NULL,27,2,'2018-06-20 20:29:53','2018-06-20 20:33:25','voyager.banner-cta.index',NULL),(27,1,'Banners','','_self','voyager-images','#000000',NULL,4,'2018-06-20 20:33:05','2018-06-20 20:33:17',NULL,''),(28,1,'Events','','_self','voyager-calendar',NULL,NULL,6,'2018-06-20 22:23:26','2018-06-24 20:32:12','voyager.events.index',NULL),(29,1,'FAQs','','_self','voyager-question',NULL,NULL,8,'2018-06-20 22:35:06','2018-06-24 20:32:12','voyager.faq.index',NULL),(30,1,'Admin Users','','_self','voyager-person','#000000',NULL,10,'2018-06-20 22:36:20','2018-06-24 20:32:12',NULL,''),(31,1,'User Members','','_self','voyager-smile','#000000',NULL,9,'2018-06-20 23:51:25','2018-06-24 20:32:12','voyager.member.index','null'),(32,1,'Static Contents','','_self','voyager-file-text','#000000',NULL,7,'2018-06-21 19:01:32','2018-06-24 20:32:12','voyager.static_content.index','null'),(33,1,'Manage Posttype','','_self','voyager-categories','#000000',32,1,'2018-06-21 19:35:43','2018-06-21 23:55:23','voyager.static-posttype.index','null'),(34,1,'Homepage','','_self',NULL,'#000000',32,2,'2018-06-21 23:36:07','2018-06-21 23:55:24','voyager.static_content.posttype','{\"type\":\"homepage\"}'),(35,1,'About','','_self',NULL,'#000000',32,3,'2018-06-21 23:37:17','2018-06-21 23:55:24','voyager.static_content.posttype','{\r\n\"type\" : \"about\"\r\n}'),(36,1,'Events','','_self',NULL,'#000000',32,4,'2018-06-21 23:38:41','2018-06-21 23:55:24','voyager.static_content.posttype','{\r\n\"type\" : \"events\"\r\n}'),(37,1,'Popup Page','','_self',NULL,'#000000',38,2,'2018-06-24 20:31:08','2018-06-24 20:32:14','voyager.pages.posttype','{\r\n\"key\" : \"popup\"\r\n}'),(38,1,'Pages','','_self','voyager-news','#000000',NULL,5,'2018-06-24 20:31:46','2018-06-24 20:32:09',NULL,'');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2018-05-29 23:27:06','2018-05-30 22:26:13'),(2,'user','2018-05-30 22:19:27','2018-05-30 22:19:27');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2016_01_01_000000_create_pages_table',2),(24,'2016_01_01_000000_create_posts_table',2),(25,'2016_02_15_204651_create_categories_table',2),(26,'2017_04_11_000000_alter_post_nullable_fields_table',2),(27,'2017_06_26_000000_create_bread_templates_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pagetype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cta_target` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (2,NULL,'Promo Berhadiah','dapatkan promo terbaru dengan hadiah yang luar binasa!','<p>lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet&nbsp;</p>','pages/June2018/9pqhk9wMgTKh5JnH2HYd.jpg','promo-berhadiah',NULL,NULL,'ACTIVE','2018-06-24 20:38:13','2018-06-25 02:32:39','popup',2),(4,NULL,'sadsadsdaasdsad','sadasd sa dsad sad ads sad','<p>sa dsa dsad sda sad sda sda sda sad&nbsp;</p>',NULL,'sadsadsdaasdsad','sad as sad sda dsa',NULL,'ACTIVE','2018-06-25 02:31:09','2018-06-25 02:37:14','normal',0);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(1,2),(2,1),(3,1),(4,1),(4,2),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(27,2),(28,1),(28,2),(29,1),(29,2),(30,1),(30,2),(31,1),(31,2),(32,1),(32,2),(33,1),(33,2),(34,1),(34,2),(35,1),(35,2),(36,1),(36,2),(37,1),(37,2),(38,1),(38,2),(39,1),(39,2),(40,1),(40,2),(41,1),(41,2),(42,1),(42,2),(43,1),(43,2),(44,1),(44,2),(45,1),(45,2),(46,1),(46,2),(47,1),(47,2),(48,1),(48,2),(49,1),(49,2),(50,1),(50,2),(51,1),(51,2),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1),(67,1),(68,1),(69,1),(70,1),(71,1),(72,1),(73,1),(74,1),(75,1),(76,1),(77,1),(78,1),(79,1),(80,1),(81,1),(82,1),(83,1),(84,1),(85,1),(86,1),(87,1),(88,1),(89,1),(90,1),(91,1),(92,1),(93,1),(94,1),(95,1),(96,1),(97,1),(98,1),(99,1),(100,1),(101,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2018-05-29 23:27:07','2018-05-29 23:27:07'),(2,'browse_bread',NULL,'2018-05-29 23:27:07','2018-05-29 23:27:07'),(3,'browse_database',NULL,'2018-05-29 23:27:07','2018-05-29 23:27:07'),(4,'browse_media',NULL,'2018-05-29 23:27:07','2018-05-29 23:27:07'),(5,'browse_compass',NULL,'2018-05-29 23:27:07','2018-05-29 23:27:07'),(6,'browse_menus','menus','2018-05-29 23:27:07','2018-05-29 23:27:07'),(7,'read_menus','menus','2018-05-29 23:27:07','2018-05-29 23:27:07'),(8,'edit_menus','menus','2018-05-29 23:27:07','2018-05-29 23:27:07'),(9,'add_menus','menus','2018-05-29 23:27:07','2018-05-29 23:27:07'),(10,'delete_menus','menus','2018-05-29 23:27:07','2018-05-29 23:27:07'),(11,'browse_roles','roles','2018-05-29 23:27:08','2018-05-29 23:27:08'),(12,'read_roles','roles','2018-05-29 23:27:08','2018-05-29 23:27:08'),(13,'edit_roles','roles','2018-05-29 23:27:08','2018-05-29 23:27:08'),(14,'add_roles','roles','2018-05-29 23:27:08','2018-05-29 23:27:08'),(15,'delete_roles','roles','2018-05-29 23:27:08','2018-05-29 23:27:08'),(16,'browse_users','users','2018-05-29 23:27:08','2018-05-29 23:27:08'),(17,'read_users','users','2018-05-29 23:27:08','2018-05-29 23:27:08'),(18,'edit_users','users','2018-05-29 23:27:08','2018-05-29 23:27:08'),(19,'add_users','users','2018-05-29 23:27:08','2018-05-29 23:27:08'),(20,'delete_users','users','2018-05-29 23:27:08','2018-05-29 23:27:08'),(21,'browse_settings','settings','2018-05-29 23:27:08','2018-05-29 23:27:08'),(22,'read_settings','settings','2018-05-29 23:27:08','2018-05-29 23:27:08'),(23,'edit_settings','settings','2018-05-29 23:27:08','2018-05-29 23:27:08'),(24,'add_settings','settings','2018-05-29 23:27:08','2018-05-29 23:27:08'),(25,'delete_settings','settings','2018-05-29 23:27:08','2018-05-29 23:27:08'),(26,'browse_hooks',NULL,'2018-05-29 23:27:10','2018-05-29 23:27:10'),(27,'browse_categories','categories','2018-05-29 23:28:11','2018-05-29 23:28:11'),(28,'read_categories','categories','2018-05-29 23:28:11','2018-05-29 23:28:11'),(29,'edit_categories','categories','2018-05-29 23:28:11','2018-05-29 23:28:11'),(30,'add_categories','categories','2018-05-29 23:28:11','2018-05-29 23:28:11'),(31,'delete_categories','categories','2018-05-29 23:28:11','2018-05-29 23:28:11'),(32,'browse_posts','posts','2018-05-29 23:28:13','2018-05-29 23:28:13'),(33,'read_posts','posts','2018-05-29 23:28:13','2018-05-29 23:28:13'),(34,'edit_posts','posts','2018-05-29 23:28:13','2018-05-29 23:28:13'),(35,'add_posts','posts','2018-05-29 23:28:13','2018-05-29 23:28:13'),(36,'delete_posts','posts','2018-05-29 23:28:13','2018-05-29 23:28:13'),(37,'browse_pages','pages','2018-05-29 23:28:14','2018-05-29 23:28:14'),(38,'read_pages','pages','2018-05-29 23:28:14','2018-05-29 23:28:14'),(39,'edit_pages','pages','2018-05-29 23:28:14','2018-05-29 23:28:14'),(40,'add_pages','pages','2018-05-29 23:28:14','2018-05-29 23:28:14'),(41,'delete_pages','pages','2018-05-29 23:28:14','2018-05-29 23:28:14'),(42,'browse_product','product','2018-05-30 20:35:33','2018-05-30 20:35:33'),(43,'read_product','product','2018-05-30 20:35:33','2018-05-30 20:35:33'),(44,'edit_product','product','2018-05-30 20:35:33','2018-05-30 20:35:33'),(45,'add_product','product','2018-05-30 20:35:33','2018-05-30 20:35:33'),(46,'delete_product','product','2018-05-30 20:35:33','2018-05-30 20:35:33'),(47,'browse_product_category','product_category','2018-05-30 20:36:20','2018-05-30 20:36:20'),(48,'read_product_category','product_category','2018-05-30 20:36:21','2018-05-30 20:36:21'),(49,'edit_product_category','product_category','2018-05-30 20:36:21','2018-05-30 20:36:21'),(50,'add_product_category','product_category','2018-05-30 20:36:21','2018-05-30 20:36:21'),(51,'delete_product_category','product_category','2018-05-30 20:36:21','2018-05-30 20:36:21'),(52,'browse_bread_templates','bread_templates','2018-05-30 21:30:46','2018-05-30 21:30:46'),(53,'read_bread_templates','bread_templates','2018-05-30 21:30:46','2018-05-30 21:30:46'),(54,'edit_bread_templates','bread_templates','2018-05-30 21:30:46','2018-05-30 21:30:46'),(55,'add_bread_templates','bread_templates','2018-05-30 21:30:46','2018-05-30 21:30:46'),(56,'delete_bread_templates','bread_templates','2018-05-30 21:30:46','2018-05-30 21:30:46'),(57,'browse_instagram','instagram','2018-06-07 22:51:10','2018-06-07 22:51:10'),(58,'read_instagram','instagram','2018-06-07 22:51:10','2018-06-07 22:51:10'),(59,'edit_instagram','instagram','2018-06-07 22:51:10','2018-06-07 22:51:10'),(60,'add_instagram','instagram','2018-06-07 22:51:10','2018-06-07 22:51:10'),(61,'delete_instagram','instagram','2018-06-07 22:51:10','2018-06-07 22:51:10'),(62,'browse_about_post','about_post','2018-06-20 19:23:05','2018-06-20 19:23:05'),(63,'read_about_post','about_post','2018-06-20 19:23:05','2018-06-20 19:23:05'),(64,'edit_about_post','about_post','2018-06-20 19:23:05','2018-06-20 19:23:05'),(65,'add_about_post','about_post','2018-06-20 19:23:05','2018-06-20 19:23:05'),(66,'delete_about_post','about_post','2018-06-20 19:23:05','2018-06-20 19:23:05'),(67,'browse_banner','banner','2018-06-20 20:25:51','2018-06-20 20:25:51'),(68,'read_banner','banner','2018-06-20 20:25:51','2018-06-20 20:25:51'),(69,'edit_banner','banner','2018-06-20 20:25:51','2018-06-20 20:25:51'),(70,'add_banner','banner','2018-06-20 20:25:51','2018-06-20 20:25:51'),(71,'delete_banner','banner','2018-06-20 20:25:51','2018-06-20 20:25:51'),(72,'browse_banner_cta','banner_cta','2018-06-20 20:29:53','2018-06-20 20:29:53'),(73,'read_banner_cta','banner_cta','2018-06-20 20:29:53','2018-06-20 20:29:53'),(74,'edit_banner_cta','banner_cta','2018-06-20 20:29:53','2018-06-20 20:29:53'),(75,'add_banner_cta','banner_cta','2018-06-20 20:29:53','2018-06-20 20:29:53'),(76,'delete_banner_cta','banner_cta','2018-06-20 20:29:53','2018-06-20 20:29:53'),(77,'browse_events','events','2018-06-20 22:23:26','2018-06-20 22:23:26'),(78,'read_events','events','2018-06-20 22:23:26','2018-06-20 22:23:26'),(79,'edit_events','events','2018-06-20 22:23:26','2018-06-20 22:23:26'),(80,'add_events','events','2018-06-20 22:23:26','2018-06-20 22:23:26'),(81,'delete_events','events','2018-06-20 22:23:26','2018-06-20 22:23:26'),(82,'browse_faq','faq','2018-06-20 22:35:06','2018-06-20 22:35:06'),(83,'read_faq','faq','2018-06-20 22:35:06','2018-06-20 22:35:06'),(84,'edit_faq','faq','2018-06-20 22:35:06','2018-06-20 22:35:06'),(85,'add_faq','faq','2018-06-20 22:35:06','2018-06-20 22:35:06'),(86,'delete_faq','faq','2018-06-20 22:35:06','2018-06-20 22:35:06'),(87,'browse_member','member','2018-06-20 23:51:25','2018-06-20 23:51:25'),(88,'read_member','member','2018-06-20 23:51:25','2018-06-20 23:51:25'),(89,'edit_member','member','2018-06-20 23:51:25','2018-06-20 23:51:25'),(90,'add_member','member','2018-06-20 23:51:25','2018-06-20 23:51:25'),(91,'delete_member','member','2018-06-20 23:51:25','2018-06-20 23:51:25'),(92,'browse_static_content','static_content','2018-06-21 19:01:31','2018-06-21 19:01:31'),(93,'read_static_content','static_content','2018-06-21 19:01:31','2018-06-21 19:01:31'),(94,'edit_static_content','static_content','2018-06-21 19:01:31','2018-06-21 19:01:31'),(95,'add_static_content','static_content','2018-06-21 19:01:31','2018-06-21 19:01:31'),(96,'delete_static_content','static_content','2018-06-21 19:01:31','2018-06-21 19:01:31'),(97,'browse_static_posttype','static_posttype','2018-06-21 19:35:43','2018-06-21 19:35:43'),(98,'read_static_posttype','static_posttype','2018-06-21 19:35:43','2018-06-21 19:35:43'),(99,'edit_static_posttype','static_posttype','2018-06-21 19:35:43','2018-06-21 19:35:43'),(100,'add_static_posttype','static_posttype','2018-06-21 19:35:43','2018-06-21 19:35:43'),(101,'delete_static_posttype','static_posttype','2018-06-21 19:35:43','2018-06-21 19:35:43');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_related`
--

DROP TABLE IF EXISTS `post_related`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_related` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_source` int(11) DEFAULT NULL,
  `post_related_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_related`
--

LOCK TABLES `post_related` WRITE;
/*!40000 ALTER TABLE `post_related` DISABLE KEYS */;
INSERT INTO `post_related` VALUES (14,1,2,'2018-06-21 00:19:02','2018-06-21 00:19:02'),(15,1,3,'2018-06-21 00:19:02','2018-06-21 00:19:02'),(18,5,3,'2018-06-21 01:10:29','2018-06-21 01:10:29');
/*!40000 ALTER TABLE `post_related` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `most_viewed` tinyint(4) DEFAULT NULL,
  `newest` tinyint(4) DEFAULT NULL,
  `click` int(11) DEFAULT NULL,
  `tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (6,1,3,'Tumbuh Alami, Alami Tumbuh',NULL,'asdasdasd asdsa dsadsda sad as dsa dsad sda sda','<p>The standard Lorem Ipsum passage, used since the 1500s</p>\r\n<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n<p>&nbsp;</p>\r\n<p>Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</p>\r\n<p>\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p>','post/June2018/Sv5u0HjviZ4IONVLk91x.jpg','tumbuh-alami-alami-tumbuh',NULL,NULL,'PUBLISHED',1,'2018-06-25 02:16:30','2018-06-25 02:16:30',NULL,1,NULL,'\"lorem,ipsum\"');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `category` int(11) NOT NULL,
  `stat` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stat` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` VALUES (1,'Website Tutorial','website-tutorial','Kumpulan tutorial tentang website','2018-05-30 20:55:03','2018-05-30 20:55:03',1),(2,'Portfolio','portfolio',NULL,'2018-05-30 20:56:21','2018-05-30 20:56:21',1);
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2018-05-29 23:27:07','2018-05-29 23:27:07'),(2,'user','Normal User','2018-05-29 23:27:07','2018-05-29 23:27:07');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo',NULL,'','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),(5,'admin.bg_image','Admin Background Image',NULL,'','image',5,'Admin'),(6,'admin.title','Admin Title','Ultra Mimi','','text',1,'Admin'),(7,'admin.description','Admin Description','Website Ultra Mimi','','text',2,'Admin'),(8,'admin.loader','Admin Loader',NULL,'','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','settings/June2018/ehC4BuXFU7lssKb6IWua.jpg','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','622543286020-cdmrpfrtde8242jb9ctm7ekgpiaq50ll.apps.googleusercontent.com','','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `static_content`
--

DROP TABLE IF EXISTS `static_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `posttype` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `param` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `content_translated` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `static_content`
--

LOCK TABLES `static_content` WRITE;
/*!40000 ALTER TABLE `static_content` DISABLE KEYS */;
INSERT INTO `static_content` VALUES (1,'1','btnBuy','[\"static_content\\/June2018\\/eaf8dcc078df1e91a055.jpg\"]',NULL,'2018-06-25 00:43:56','2018-06-25 00:43:56','images');
/*!40000 ALTER TABLE `static_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `static_posttype`
--

DROP TABLE IF EXISTS `static_posttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static_posttype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `static_posttype`
--

LOCK TABLES `static_posttype` WRITE;
/*!40000 ALTER TABLE `static_posttype` DISABLE KEYS */;
INSERT INTO `static_posttype` VALUES (1,'Homepage','2018-06-21 19:39:00','2018-06-21 19:46:33','homepage'),(2,'About','2018-06-21 19:39:00','2018-06-21 19:46:28','about'),(3,'Events','2018-06-21 19:40:00','2018-06-21 19:46:24','events');
/*!40000 ALTER TABLE `static_posttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',5,'pt','Post','2018-05-29 23:28:15','2018-05-29 23:28:15'),(2,'data_types','display_name_singular',6,'pt','Página','2018-05-29 23:28:15','2018-05-29 23:28:15'),(3,'data_types','display_name_singular',1,'pt','Utilizador','2018-05-29 23:28:15','2018-05-29 23:28:15'),(4,'data_types','display_name_singular',4,'pt','Categoria','2018-05-29 23:28:15','2018-05-29 23:28:15'),(5,'data_types','display_name_singular',2,'pt','Menu','2018-05-29 23:28:15','2018-05-29 23:28:15'),(6,'data_types','display_name_singular',3,'pt','Função','2018-05-29 23:28:15','2018-05-29 23:28:15'),(7,'data_types','display_name_plural',5,'pt','Posts','2018-05-29 23:28:15','2018-05-29 23:28:15'),(8,'data_types','display_name_plural',6,'pt','Páginas','2018-05-29 23:28:15','2018-05-29 23:28:15'),(9,'data_types','display_name_plural',1,'pt','Utilizadores','2018-05-29 23:28:15','2018-05-29 23:28:15'),(10,'data_types','display_name_plural',4,'pt','Categorias','2018-05-29 23:28:15','2018-05-29 23:28:15'),(11,'data_types','display_name_plural',2,'pt','Menus','2018-05-29 23:28:15','2018-05-29 23:28:15'),(12,'data_types','display_name_plural',3,'pt','Funções','2018-05-29 23:28:15','2018-05-29 23:28:15'),(13,'categories','slug',1,'pt','categoria-1','2018-05-29 23:28:15','2018-05-29 23:28:15'),(14,'categories','name',1,'pt','Categoria 1','2018-05-29 23:28:15','2018-05-29 23:28:15'),(15,'categories','slug',2,'pt','categoria-2','2018-05-29 23:28:15','2018-05-29 23:28:15'),(16,'categories','name',2,'pt','Categoria 2','2018-05-29 23:28:15','2018-05-29 23:28:15'),(17,'pages','title',1,'pt','Olá Mundo','2018-05-29 23:28:15','2018-05-29 23:28:15'),(18,'pages','slug',1,'pt','ola-mundo','2018-05-29 23:28:16','2018-05-29 23:28:16'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2018-05-29 23:28:16','2018-05-29 23:28:16'),(20,'menu_items','title',1,'pt','Painel de Controle','2018-05-29 23:28:16','2018-05-29 23:28:16'),(21,'menu_items','title',2,'pt','Media','2018-05-29 23:28:16','2018-05-29 23:28:16'),(22,'menu_items','title',13,'pt','Publicações','2018-05-29 23:28:16','2018-05-29 23:28:16'),(23,'menu_items','title',3,'pt','Utilizadores','2018-05-29 23:28:16','2018-05-29 23:28:16'),(24,'menu_items','title',12,'pt','Categorias','2018-05-29 23:28:16','2018-05-29 23:28:16'),(25,'menu_items','title',14,'pt','Páginas','2018-05-29 23:28:16','2018-05-29 23:28:16'),(26,'menu_items','title',4,'pt','Funções','2018-05-29 23:28:16','2018-05-29 23:28:16'),(27,'menu_items','title',5,'pt','Ferramentas','2018-05-29 23:28:16','2018-05-29 23:28:16'),(28,'menu_items','title',6,'pt','Menus','2018-05-29 23:28:16','2018-05-29 23:28:16'),(29,'menu_items','title',7,'pt','Base de dados','2018-05-29 23:28:16','2018-05-29 23:28:16'),(30,'menu_items','title',10,'pt','Configurações','2018-05-29 23:28:16','2018-05-29 23:28:16');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Admin','admin@admin.com','users/May2018/LnxbABvzbELtiwSiaXBp.jpg','$2y$10$rd5l6Mg9IjQ6919QUpJaDuMhugvNAGExGwi/Ct2u4s8CVeLLscqjy','4b13FxGm8uqkr46QcKJv63R2tPAqK7PnVq3iJuLQoKEHxfKsoEy4ICd6sjm4','{\"locale\":\"en\"}','2018-05-29 23:28:12','2018-05-30 20:31:37'),(2,2,'user','user@localhost.com','users/default.png','$2y$10$bleiTSDdsEY8XgE.gLF0z.VFazg/5TfcFDrHm8C.cHkW2M1Vit8yq',NULL,'{\"locale\":\"en\"}','2018-05-30 22:22:37','2018-05-30 22:22:37'),(3,1,'Admin UMIMI','admin@ultramimi.com','users/default.png','$2y$10$dvF9/mbPDK3TTYxPIuSoPOz9xUo4IUUcAL0o6D65mJUhlHYpyFQ7e',NULL,NULL,'2018-06-24 20:02:42','2018-06-24 20:02:42');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-25 18:39:05
