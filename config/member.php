<?php
return [
	'meta' => [
		'Full Name' => [
			'type' => 'text',
			'name' => 'fullname',
			'attr' => ['class' => 'form-control'],
			'visible' => true,
		],
		'Gender' => [
			'type' => 'select',
			'list' => [
				'L' => 'Laki-laki',
				'P' => 'Perempuan'
			],
			'name' => 'gender',
			'attr' => ['class' => 'form-control'],
			'visible' => true,			
		],
		'Date of Birth' => [
			'type' => 'date',
			'name' => 'date_of_birth',
			'attr' => ['class' => 'form-control date-of-birth'],
			'visible' => true,
		],
		'Address' => [
			'type' => 'textarea',
			'name' => 'address',
			'attr' => ['class' => 'form-control'],
			'visible' => false,
		],
		'User Photo' => [
			'type' => 'image',
			'name' => 'userphoto',
			'attr' => ['class' => 'form-control'],
			'visible' => true
		]
	],
];