<?php

namespace Module\Member;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class MemberServiceProvider extends ServiceProvider
{
    protected $namespace = 'Module\Member\Controllers';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->router->group(['namespace' => $this->namespace, 'middleware' => ['web', 'admin.user']], function ($route) {
            require realpath(__DIR__.'/Routes/web.php');
        });

        $this->app->singleton('member-facade', function ($app) {
            return new Services\MemberService($app);
        });

        $this->loadViewsFrom(realpath(__DIR__.'/Views/'), 'member');
    }
}
