<?php
namespace Module\Member\Models;

use Illuminate\Database\Eloquent\Model;

class MemberMeta extends Model
{
    //
    protected $table = "member_meta";
 	
 	protected $fillable = [
 		'member_id',
 		'meta',
 		'value'
 	];
}
