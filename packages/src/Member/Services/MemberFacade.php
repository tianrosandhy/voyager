<?php
namespace Module\Member\Services;
use Illuminate\Support\Facades\Facade;
/**
 * @see \Illuminate\Foundation\Application
 */
class MemberFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'member-facade';
    }
}
