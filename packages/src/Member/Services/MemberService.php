<?php

namespace Module\Member\Services;

use Illuminate\Contracts\Foundation\Application;
use Module\Member\Models\Member;
use Module\Member\Models\MemberMeta;

class MemberService
{	
	public 
		$email,
		$mailname,
		$host,
		$message;

	public function __construct(Application $app){
		$this->app = $app;
	}
	
	public function userMetas($limiter = []){
		if(empty($limiter)){
			$data = MemberMeta::get();
		}
		else{
			$data = MemberMeta::whereIn('member_id', $limiter)->get();
		}
		$out = [];
		foreach($data as $row){
			$out[$row->member_id][$row->meta] = $row->value;
		}
		return $out;
	}


	public function emailValidate($email=''){
		$this->email = $email;
		$out = self::setEmail();
		return $out;
	}

	protected function check_format(){
    	if(filter_var($this->email, FILTER_VALIDATE_EMAIL)){
    		return true;
    	}
    	return false;
    }




    //from mail helper
    protected function setEmail(){
        //preparing email validation lists
    	if(self::check_format()){
    		$exp = explode("@", $this->email);
	    	$this->mailname = $exp[0];
	    	$this->host = $exp[1];
    	}
    	else{
    		$this->message = 'Invalid email format.';
    	}

    	if(!$this->check_dns()){
    		$this->message = 'Mail DNS "'.self::$host.'" provider not found';
    	}

    	if($this->check_disposable()){
    		$this->message = 'The email that you entered is listed in blacklist';
    	}

    	self::reformat_email();

    	return [
    		'email' => $this->email,
    		'error' => $this->message
    	];

    }

    protected function reformat_email(){
    	//mengabaikan tanda titik dan seluruh alphabet setelah tanda +
    	//hanya untuk host yang ditandai untuk diformat ulang
    	if(in_array($this->host, ['gmail.com'])){
    		$this->mailname = str_replace(".", "", $this->mailname);

    		$n = strpos($this->mailname, "+");
    		if($n){
    			$this->mailname = substr($this->mailname, 0, $n);
    		}
    	}
    	$this->email = $this->mailname."@".$this->host;
    }


    protected function check_dns(){
    	if(strlen($this->host) > 0){
	    	if(checkdnsrr($this->host, "MX")){
	    		return true;
	    	}
	    	return false;
    	}
    	else
    		return false;
    }

    protected function check_disposable(){
    	$data = config('blacklist.mailhost');
    	$list = preg_split("/[\r\n]+/", $data);

    	if(in_array($this->host, $list)){
    		return true;
    	}
    	return false;

    }

}