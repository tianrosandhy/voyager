<div class="form-group">
    <label for="status">Related To</label>
    <select class="form-control select2" multiple="multiple" name="related_to[]">
    	@foreach($relatedArticle['list'] as $rel)
    	<option value="{{ $rel->id }}" {{ !empty($relatedArticle['selected']) ? ($relatedArticle['selected']->pluck('post_related_id')->contains($rel->id) ? 'selected' : '') : '' }}>{{ $rel->title }}</option>
    	@endforeach
    </select>
</div>