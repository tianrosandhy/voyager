<?php
namespace Module\Post\Models;

use Illuminate\Database\Eloquent\Model;

class PostRelatedModel extends Model
{
    //
    protected $table = "post_related";
    
    protected $fillable = [
    	'post_source',
    	'post_related_id'
    ];


    public function postData(){
    	return $this->belongsTo('Module\Post\Models\PostModel', 'post_related_id');
    }
}
