<?php

namespace Module\Post\Services;

use Illuminate\Contracts\Foundation\Application;
use Module\Post\Models\PostModel;
use Module\Post\Models\PostRelatedModel;

class PostService
{	
	public function __construct(Application $app){
		$this->app = $app;
	}

	public function relatedArticle($id=null){
		$data = PostRelatedModel::with('postData')->where('post_source', $id)->get();
		if(!empty($data)){
			if(!empty($data->pluck('postData'))){
				return $data->pluck('postData')->pluck('title', 'id');
			}
		}
		return $data;
	}
	
}