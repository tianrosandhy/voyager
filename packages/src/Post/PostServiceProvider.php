<?php

namespace Module\Post;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Module\Post\Services\PostService;

class PostServiceProvider extends ServiceProvider
{
    protected $namespace = 'Module\Post\Controllers';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->router->group(['namespace' => $this->namespace, 'middleware' => ['web', 'admin.user']], function ($route) {
            require realpath(__DIR__.'/Routes/web.php');
        });

        $this->app->singleton('post-facade', function ($app) {
            return new PostService($app);
        });

        $this->loadViewsFrom(realpath(__DIR__.'/Views/'), 'post');
    }
}
