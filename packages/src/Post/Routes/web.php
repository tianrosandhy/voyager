<?php
Route::group(['prefix' => env('ADMIN_PREFIX')], function($route){
	$route->post('post/switch', 'PostController@switch')->name('voyager.post.switch');
});