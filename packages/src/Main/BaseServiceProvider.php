<?php
namespace Module\Main;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class BaseServiceProvider extends ServiceProvider
{
    public 
    	$namespace = 'Module\Main\Controllers',
     	$hint = 'main',
     	$dir = __DIR__;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->router->group(['namespace' => $this->namespace, 'middleware' => 'web'], function ($route) {
            require realpath($this->dir.'/Routes/web.php');
        });

        $this->loadViewsFrom(realpath($this->dir.'/Views/'), $this->hint);
    }


}
