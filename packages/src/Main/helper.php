<?php
function admin_url($target=''){
	return url(env('ADMIN_PREFIX'). '/'.$target);
}

function arrToSingle($asset=''){
	$json = json_decode($asset);
	return $json[0];
}


function static_content($param, $trans=false, $strict=false){
	//pecah type
	$var = explode('.', $param);
	if(count($var) == 2){
		return StaticContent::out($var[0], $var[1], $trans, $strict);
	}
}

function static_type($param){
	//pecah type
	$var = explode('.', $param);
	if(count($var) == 2){
		return StaticContent::type($var[0], $var[1]);
	}
}

function brokenImage(){
	return asset('img/broken-image-temporary.png');
}

function table_structure($name){
	return \TCG\Voyager\Models\DataType::with('rows')
		->where('name', $name)
		->first();
}

function row_detail($table_structure, $rowname){
	if(is_string($table_structure)){
		$table_structure = table_structure($table_structure); //buat instance kalau berupa string
	}

	$list = $table_structure->rows;
	$list = $list->where('field', $rowname)->first();

	$details = $list->details;
	if(strlen($details) > 0){
		return json_decode($details);
	}
	return [];
}

function model_instance($name){
	$data = \TCG\Voyager\Models\DataType::where('name', $name)->first();
	if(!empty($data)){
		$model = $data->model_name;
		return app($model);
	}
	//kalau model instancenya nggak ada, artinya tabel mentah
	return \DB::table($name);
}

function api_response($type, $data, $status=200){
	return response()->json([
		'type' => $type,
		'message' => $data
	], $status);
}

// HELPER untuk convert URL normal Youtube jadi embed 
function youtube_id($url){
	preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);
	return $matches;
}

function embed_url($youtube_link){
	$id = youtube_id($youtube_link);
	if(count($id) > 0){
		$format = "https://www.youtube.com/embed/".$id[0];
		return $format;
	}
	return null;
}
