<?php

namespace Module\Main;

use Module\Main\BaseServiceProvider;

class MainServiceProvider extends BaseServiceProvider
{

    public
        $namespace = 'Module\Main\Controllers',
        $hint = 'main',
        $dir = __DIR__;

}
