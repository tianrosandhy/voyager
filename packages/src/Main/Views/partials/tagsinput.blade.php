@section('head')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/tagsinput/bootstrap-tagsinput.css') }}">
    <style>
    	.bootstrap-tagsinput{width:100%;}
    </style>
@endsection


@section ('javascript')
	@parent
    <script src="{{ asset('vendor/tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script>
    $(window).on('load',function(){
    	$(".tagsinput").tagsinput();
    });        
    </script>
@stop