@if($meta['type'] == 'select')
	<select name="{{ $meta['name'] }}"
		@foreach($meta['attr'] as $prm => $vll)
			{{ $prm }}="{{$vll}}"
		@endforeach
	>
		<option value=""></option>
		@foreach($meta['list'] as $prm => $vll)
		<option value="{{ $prm }}" {{ $prm == $value ? 'selected' : '' }}>{{ $vll }}</option>
		@endforeach
	</select>
@elseif($meta['type'] == 'textarea')
	<textarea name="{{ $meta['name'] }}"
		@foreach($meta['attr'] as $prm => $vll)
			{{ $prm }}="{{$vll}}"
		@endforeach
	>{!! $value !!}</textarea>
@elseif($meta['type'] == 'image')
	@if($value)
		<?php $value = json_decode($value); ?>
		<img src="{{ Storage::url($value[0]) }}" height=100>
	@endif
	<input type="file" name="{{ $meta['name'] }}" accept="image/*"
		@foreach($meta['attr'] as $prm => $vll)
			{{ $prm }}="{{$vll}}"
		@endforeach
	>
@else
	<input type="text" name="{{ $meta['name'] }}"
		@foreach($meta['attr'] as $prm => $vll)
			{{ $prm }}="{{$vll}}"
		@endforeach
		value="{{ $value }}"
	>
@endif