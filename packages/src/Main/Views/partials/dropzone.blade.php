@section ('head')
@parent
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/css/dropzone.css') }}">
@stop

@section ('javascript')
@parent
<script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>
<script>
//dropzone instance
Dropzone.autoDiscover = false;
if($(".mydropzone").length){
	refreshDropzone();
}

function refreshDropzone(){
	$(".mydropzone").each(function(){
		var ajaxurl = $(this).data("target");
		var dropzonehash = $(this).attr('data-hash');

		if($(this).find('.dz-default').length == 0){
			$(this).dropzone({
				url : ajaxurl,
				sending : function(file, xhr, formData){
					formData.append("_token", $("meta[name='csrf-token']").attr('content'));
				},
				init : function(){
					this.on("success", function(file, data){
						$(".dropzone_uploaded[data-hash='"+dropzonehash+"']").val(data).change();
						this.removeFile(this.files[0]);
					});
					this.on("addedfile", function() {
				      if (this.files[1]!=null){
				        this.removeFile(this.files[0]);
				      }
				    });
				}
			});		
		}
		
	});	
}

</script>
@stop