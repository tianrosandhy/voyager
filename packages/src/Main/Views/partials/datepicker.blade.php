@section ('head')
	@parent
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/css/datepicker.css') }}">
@endsection

@section ('javascript')
	@parent
	<script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
	<script>
	$(function(){
		$(".datepicker").datepicker();
		$(".date-of-birth").datepicker({
			startView: 2
		});
		$(".date-of-birth").attr('readonly', 'readonly');
	});
	</script>
@endsection