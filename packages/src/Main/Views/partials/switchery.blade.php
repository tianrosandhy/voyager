@section('head')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/switchery/css/switchery.min.css') }}">
@endsection


@section ('javascript')
	@parent
    <script src="{{ asset('vendor/switchery/js/switchery.min.js') }}"></script>
    <script>
    $(window).on('load',function(){
        $('[data-init="switchery"]').each(function() {
            var el = $(this);
            new Switchery(el.get(0), {
                size : el.data("size")
            });
        });        
    });        
    </script>
@stop