<?php

namespace Module\About;

use Module\Main\BaseServiceProvider;

class AboutServiceProvider extends BaseServiceProvider
{

    public
        $namespace = 'Module\About\Controllers',
        $hint = 'about',
        $dir = __DIR__;

}
