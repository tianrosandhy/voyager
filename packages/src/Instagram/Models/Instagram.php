<?php
namespace Module\Instagram\Models;

use Illuminate\Database\Eloquent\Model;

class Instagram extends Model
{
    //
    protected $table = "instagram";
    public $timestamps = false;
    
}
