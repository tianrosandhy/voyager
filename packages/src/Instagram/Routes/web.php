<?php
Route::group(['prefix' => env('ADMIN_PREFIX')], function($route){
	$route->get('instagram/fetch', 'InstagramController@fetchData')->name('voyager.instagram.fetch');
	$route->post('instagram/switch', 'InstagramController@switch')->name('voyager.instagram.switch');
});