<div class="table-responsive">
    <table id="dataTable" class="table table-hover">
        <thead>
            <tr>
                @can('delete',app($dataType->model_name))
                    <th>
                        <input type="checkbox" class="select_all">
                    </th>
                @endcan
                @foreach($dataType->browseRows as $row)
                <th>
                    {{ $row->display_name }}
                </th>
                @endforeach
                <th class="actions">
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($filtered as $row)
            <tr>
                @can('delete',app($dataType->model_name))
                    <td>
                        <input type="checkbox" name="row_id" id="checkbox_{{ $row->id }}" value="{{ $row->id }}">
                    </td>
                @endcan
                <td>{{ $row->pagetype }}</td>
                <td>{{ $row->title }}</td>
                <td><input type="checkbox" data-init="switchery" name="status" data-size="small" class="js-switch" data-id="{{ $row->id }}" {{ $row->status == 'ACTIVE' ? 'checked' : '' }}></td>
                <td>{{ $row->created_at }}</td>
                <td>
                    <img src="{{ Storage::url($row->image) }}" height=50>
                </td>
                <td>
                    <?php
                        $target = json_decode($row->cta_target);
                        if(is_array($target)){
                            foreach($target as $btntarget){
                                $bnn = Banner::ctaInstance($btntarget);
                                if($bnn){
                                    echo '<span class="label label-primary">'.$bnn->label.'</span> ';
                                }
                            }
                        }
                    ?>
                </td>

                <td class="no-sort no-click" id="bread-actions">
                    <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="{{ $row->id }}" id="delete-{{ $row->id }}">
                        <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                    </a>

                    <a href="{{ url()->route('voyager.pages.index') }}/{{ $row->id }}/edit" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                        <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                    </a>

                    <a href="{{ url()->route('voyager.pages.index') }}/{{ $row->id }}" title="View" class="btn btn-sm btn-warning pull-right view">
                        <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm"> View</span>
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>