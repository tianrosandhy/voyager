<?php

namespace Module\Page;

use Module\Main\BaseServiceProvider;

class PageServiceProvider extends BaseServiceProvider
{

    public
        $namespace = 'Module\Page\Controllers',
        $hint = 'pages',
        $dir = __DIR__;

}
