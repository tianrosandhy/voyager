<?php
namespace Module\Page\Controllers;

use Module\Main\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use Storage;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use Module\Page\Models\PageModel;


class PageController extends AdminBaseController
{
	public 
		$request,
		$allowed = [
			'normal',
			'popup'
		];

	public function __construct(Request $req){
		$this->request = $req;
		$this->hint = 'pages';
	}


	public function index(Request $request, $pagetype=''){
		// GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->hint;

        if(strlen($pagetype) == 0){
	        $pagetype = $this->request->pagetype;
        }
        $filtered = self::checkPageType($pagetype);
        if($filtered === false){
        	return redirect()->route('voyager.dashboard')->withErrors('Page not found');
        }

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();


        $view = 'main::browse';
        if (view()->exists($this->hint."::browse")) {
            $view = $this->hint."::browse";
        }

        $hint = $this->hint;
    	$viewData = compact(
    		'dataType',
    		'hint',
    		'filtered',
    		'pagetype'
    	);

		return Voyager::view($view, $viewData);   
	}


	protected function checkPageType($pagetype){
		if(strlen($pagetype) > 0){
			$cek = PageModel::where('pagetype', $pagetype)->get();
		}
		else{
			$cek = PageModel::get();
		}
		return $cek;
	}




	public function create(Request $request, $type="")
    {
        $slug = $this->hint;

        if(strlen($type) > 0){
        	if(!in_array($type, $this->allowed)){
        		return redirect()->route('voyager.pages.index')->withErrors('page type not found');
        	}
        }
        else{
        	$type = 'normal';
        }

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
                            ? new $dataType->model_name()
                            : false;

        foreach ($dataType->addRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'main::edit-add';

        if (view()->exists($this->hint."::edit-add")) {
            $view = $this->hint."::edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'type'));
    }


    public function store(Request $request)
    {
        $slug = $this->hint;

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate')) {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
            event(new BreadDataAdded($dataType, $data));

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            return redirect()
                    ->route('voyager.pages.posttype', ['type' => $request->pagetype])
                    ->with([
                        'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                        'alert-type' => 'success',
                    ]);
        }
    }




    public function edit(Request $request, $id)
    {
        $slug = $this->hint;

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->with($relationships)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'main::edit-add';
        $type = $dataTypeContent->pagetype;
        if (view()->exists($this->hint."::edit-add")) {
            $view = $this->hint."::edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'type'));
    }

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = $this->hint;

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            return redirect()
                    ->route('voyager.pages.posttype', ['type' => $request->pagetype])
                    ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

}