<?php
Route::group(['prefix' => env('ADMIN_PREFIX')], function($route){
	//post type
	$route->get('pages/type/{type}', 'PageController@index')->name('voyager.pages.posttype');
	$route->get('pages/create/{type}', 'PageController@create');

	$route->post('pages/switch', 'PageController@switch')->name('voyager.pages.switch');
});