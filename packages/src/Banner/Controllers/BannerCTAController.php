<?php
namespace Module\Banner\Controllers;

use Module\Main\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use Storage;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;

use Module\Banner\Models\BannerToCTA;
use Module\Banner\Models\BannerCTA;

use Module\Page\Models\PageModel;
use Module\Post\Models\PostModel;


class BannerCTAController extends AdminBaseController
{
	public 
		$request, 
		$allowed = [
			'page',
			'post',
			'popup'
		];

	public function __construct(Request $req){
		$this->request = $req;
		$this->hint = 'banner-cta';
	}




	public function linkToType($type=''){
		if(strlen($type) == 0 || !in_array($type, $this->allowed)){
			return ['type' => 'error', 'message' => 'CTA Type not found'];
		}

		if($type == 'page'){
			$data = PageModel::where([
				'pagetype' => 'normal',
				'status' => 'ACTIVE'
			])->get(['id', 'title']);
		}
		if($type == 'popup'){
			$data = PageModel::where([
				'pagetype' => 'popup',
				'status' => 'ACTIVE'
			])->get(['id', 'title']);
		}
		if($type == 'post'){
			$data = PostModel::where([
				'status' => 'PUBLISHED'
			])->get(['id', 'title']);
		}


		$instance_id = $this->request->id;
		$selected = '';
		if($instance_id > 0){
			$instance = BannerCTA::find($instance_id);
			$selected = $instance->cta_target;
		}


		$view = '<select name="cta_target" class="form-control">';
			$view .= '<option></option>';
		foreach($data as $row){
			$view .= '<option value="'.$row->id.'" '.($row->id == $selected ? 'selected' : '').' >'.$row->title.'</option>';
		}
		$view .= '</select>';

		return [
			'raw' => $data,
			'html' => $view
		];

	}






}