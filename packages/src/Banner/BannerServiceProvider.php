<?php

namespace Module\Banner;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Module\Banner\Services\BannerService;

class BannerServiceProvider extends ServiceProvider
{
    protected $namespace = 'Module\Banner\Controllers';

    public function register()
    {
        //
        $this->app->router->group(['namespace' => $this->namespace, 'middleware' => ['web', 'admin.user']], function ($route) {
            require realpath(__DIR__.'/Routes/web.php');
        });

        $this->app->singleton('banner-facade', function($app){
            return new BannerService($app);
        });

        $this->loadViewsFrom(realpath(__DIR__.'/Views/banner'), 'banner');
        $this->loadViewsFrom(realpath(__DIR__.'/Views/banner-cta'), 'banner-cta');
    }
}
