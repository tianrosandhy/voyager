<?php
Route::group(['prefix' => env('ADMIN_PREFIX')], function($route){
	$route->post('banner/switch', 'BannerController@switch')->name('voyager.banner.switch');

	$route->post('banner-cta/link-to-type/{type?}', 'BannerCTAController@linkToType')->name('voyager.banner.link');
});