<?php
namespace Module\Banner\Models;

use Illuminate\Database\Eloquent\Model;

class BannerCTA extends Model
{
    //
    protected $table = "banner_cta";
    
    public function listBanner(){
    	return $this->belongsTo('Module\Banner\Models\BannerToCTA', 'banner_cta_id');
    }
}
