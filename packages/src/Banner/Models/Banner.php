<?php
namespace Module\Banner\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    protected $table = "banner";

    public function listCta(){
    	return $this->hasMany('Module\Banner\Models\BannerToCTA', 'banner_id');
    }
    
}
