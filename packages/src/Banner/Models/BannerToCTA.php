<?php
namespace Module\Banner\Models;

use Illuminate\Database\Eloquent\Model;

class BannerToCTA extends Model
{
    //
    protected $table = "banner_to_cta";

    protected $fillable = [
    	'banner_id',
    	'banner_cta_id'
    ];
    

    public function banner(){
    	return $this->belongsTo('Module\Banner\Models\Banner', 'banner_id');
    }

    public function cta(){
    	return $this->belongsTo('Module\Banner\Models\BannerCTA', 'banner_cta_id');
    }
}
