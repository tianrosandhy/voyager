<?php
namespace Module\Banner\Services;
use Illuminate\Support\Facades\Facade;
/**
 * @see \Illuminate\Foundation\Application
 */
class BannerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'banner-facade';
    }
}
