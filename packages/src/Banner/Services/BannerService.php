<?php

namespace Module\Banner\Services;

use Illuminate\Contracts\Foundation\Application;
use Module\Banner\Models\Banner;
use Module\Banner\Models\BannerCTA;
use Module\Banner\Models\BannerToCTA;

use Module\Page\Models\PageModel;
use Module\Post\Models\PostModel;


class BannerService
{	
	public function __construct(Application $app){
		$this->app = $app;
	}

	public function getCta($id=null){
		$data = BannerToCTA::with('cta')->where('banner_id', $id)->get();
		if(!empty($data)){
			if(!empty($data->pluck('cta'))){
				return $data->pluck('cta');
			}
		}
		return $data;
	}

	public function ctaUrl($type, $id=0){
		if($type == 'post'){
			$get = PostModel::find($id);
		}
		else{
			$get = PageModel::find($id);
		}

		if(!empty($get)){
			return [
				'title' => $get->title,
				'slug' => $get->slug
			];
		}
		return false;
	}

	public function ctaList($selected=''){
		$selected = json_decode($selected, true);

		$data = BannerCTA::get(['id', 'title', 'label']);
		$out = [];

		//selected data dioutput duluan
		foreach($selected as $sel){
			$instance = BannerCTA::find($sel);
			$out[] = [
				'id' => $instance->id,
				'title' => $instance->title . '('.$instance->label.')',
				'selected' => 'selected'
			];
		}

		foreach($data as $row){
			if(!in_array($row->id, $selected)){
				$out[] = [
					'id' => $row->id,
					'title' => $row->title .' ('.$row->label.')',
					'selected' => '' 
				];
			}
		}
		return $out;
	}
	
	public function ctaInstance($id){
		$data = BannerCTA::find($id);
		return $data;
	}
}