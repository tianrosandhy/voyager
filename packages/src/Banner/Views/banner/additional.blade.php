<div class="form-group">
    <label for="status">Click To Actions</label>
    <select class="form-control multiselect" multiple="multiple" name="cta_id[]">
    	@foreach($ctaButtons['list'] as $rel)
    	<option value="{{ $rel->id }}" {{ !empty($ctaButtons['selected']) ? ($ctaButtons['selected']->pluck('banner_cta_id')->contains($rel->id) ? 'selected' : '') : '' }}>{{ $rel->title }} - {{ $rel->label }}</option>
    	@endforeach
    </select>
</div>