@extends('main::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp


                            <div class="form-group">
                            	<label>CTA Name</label>
                            	<input type="text" name="title" class="form-control" value="{{ $dataTypeContent->title }}">
                            </div>
                            <div class="form-group">
                            	<label>CTA Label</label>
                            	<input type="text" name="label" class="form-control" value="{{ $dataTypeContent->label }}">
                            </div>
                            <div class="form-group">
                            	<label>CTA Type</label>
                            	@php
                            	$type = $dataTypeRows->where('field', 'type')->first();
                            	if(!empty($type)){
                            		$opts = json_decode($type->details);
	                            }
                            	@endphp
                            	<select name="type" class="form-control cta-select-type" data-old-type="{{ $dataTypeContent->type }}" data-old-val="{{ $dataTypeContent->id }}">
                            		@if(isset($opts))
                            		@foreach($opts->options as $parm => $vals)
                            		<option value="{{ $parm }}" {{ 
                            			strlen($dataTypeContent->type) > 0 ? 
                            			($parm == $dataTypeContent->type ? 'selected' : '') : 
                            			($parm == $opts->default ? 'selected' : '') }}
                            		>{{ $vals }}</option>
                            		@endforeach
                            		@endif
                            	</select>
                            </div>

                            <div class="form-group" data-control="target">
                            	<label>Target</label>
                            	<div>
                            		<select name="cta_target" class="form-control"></select>
                            	</div>
                            </div>

                            <div class="form-group" data-control="url">
                            	<label>Full URL</label>
                            	<input class="form-control" type="text" name="url" placeholder="Ex : https://www.example.com">
                            </div>


                            


                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
	@parent
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();



            cta_switcher();
            $(".cta-select-type").on('change', function(){
            	cta_switcher();
            });

        });

        function cta_switcher(){
            cta_selected = $(".cta-select-type").val();
            if(cta_selected == 'custom'){
            	$("[data-control='target']").slideUp();
            	$("[data-control='url']").slideDown();
            }
            else{
            	$("[data-control='target']").slideDown();
            	$("[data-control='url']").slideUp();
            }

            //manage old val only if selected type = data-old-type
            forid = 0;
            if($(".cta-select-type").attr('data-old-type') == cta_selected){
            	forid = $(".cta-select-type").attr('data-old-val');
            }

            loadByType(cta_selected, forid);
        }

        function loadByType(type, dataid){
        	dataid = dataid || 0;
        	$.ajax({
        		url : '{{ url()->route('voyager.banner.link') }}' + '/' + type,
        		type : "POST",
        		dataType : "json",
        		data : {
        			id : dataid
        		},
        		success : function(resp){
        			$("[data-control='target'] > div").html(resp.html);
        		},
        		error : function(resp){

        		}
        	});
        }
    </script>
@stop
