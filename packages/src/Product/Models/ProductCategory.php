<?php
namespace Module\Product\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
	public $table = "product_category";    
}
