<?php
Route::group(['prefix' => env('ADMIN_PREFIX')], function($route){
	$route->post('product/switch', 'ProductController@switch')->name('voyager.product.switch');
	$route->post('product-category/switch', 'ProductCategoryController@switch')->name('voyager.product-category.switch');
});
