<?php
namespace Module\Product\Controllers;

use Module\Main\Controllers\AdminBaseController;
use Illuminate\Http\Request;

class ProductController extends AdminBaseController
{
	public $request;

	public function __construct(Request $req){
		$this->request = $req;
		$this->hint = 'product';
	}

}