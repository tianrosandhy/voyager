<?php

namespace Module\Product;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider
{
    protected $namespace = 'Module\Product\Controllers';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->router->group(['namespace' => $this->namespace, 'middleware' => ['web', 'admin.user']], function ($route) {
            require realpath(__DIR__.'/Routes/web.php');
        });
    }
}
