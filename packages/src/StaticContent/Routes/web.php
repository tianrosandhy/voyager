<?php
Route::group(['prefix' => env('ADMIN_PREFIX')], function($route){
	//post type
	$route->get('static_content/type/{type}', 'StaticContentController@index')->name('voyager.static_content.posttype');
	$route->get('static_content/create/{type}', 'StaticContentController@create');
});