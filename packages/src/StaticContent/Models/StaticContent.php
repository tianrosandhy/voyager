<?php
namespace Module\StaticContent\Models;

use Illuminate\Database\Eloquent\Model;

class StaticContent extends Model
{
    //
    protected $table = "static_content";
    
    protected $fillable = [
    	'posttype',
    	'param',
    	'content',
    	'content_translated',
    	'type'
    ];
}
