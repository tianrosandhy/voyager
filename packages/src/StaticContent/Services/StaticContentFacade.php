<?php
namespace Module\StaticContent\Services;
use Illuminate\Support\Facades\Facade;
/**
 * @see \Illuminate\Foundation\Application
 */
class StaticContentFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'static_content-facade';
    }
}
