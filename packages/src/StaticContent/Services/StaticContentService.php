<?php

namespace Module\StaticContent\Services;

use Illuminate\Contracts\Foundation\Application;
use Module\StaticContent\Models\StaticContent;
use Module\StaticContent\Models\StaticPosttype;

class StaticContentService
{	
	public function __construct(Application $app){
		$this->app = $app;
	}

	public function getPosttype($selected=null){
		return StaticPosttype::get();
	}

	public function posttype($id){
		return StaticPosttype::find($id)->title;
	}
	
	public function posttypeInstance($val, $type='slug'){
		$data = StaticPosttype::where($type, $val)->first();
		return $data;
	}
}