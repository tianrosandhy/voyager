<div class="table-responsive">
    <table id="dataTable" class="table table-hover">
        <thead>
            <tr>
                @can('delete',app($dataType->model_name))
                    <th>
                        <input type="checkbox" class="select_all">
                    </th>
                @endcan
                @foreach($dataType->browseRows as $row)
                <th>
                    {{ $row->display_name }}
                </th>
                @endforeach
                <th class="actions">
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($filtered as $row)
            <tr>
                @can('delete',app($dataType->model_name))
                    <td>
                        <input type="checkbox" name="row_id" id="checkbox_{{ $row->id }}" value="{{ $row->id }}">
                    </td>
                @endcan
                <td>{{ $row->param }}</td>
                <td>{{ StaticContent::posttype($row->posttype) }}</td>
                <td>{{ $row->type }}</td>
                <td>
                    @if($row->type == 'images')
                        @php
                        $imgData = json_decode($row->content);
                        foreach($imgData as $img){
                            echo '<img src="'.Storage::url($img).'" height=50>';
                        }
                        @endphp
                    @else
                        {{ $row->content }}                    
                    @endif
                </td>
                <td>{{ $row->created_at }}</td>
                <td class="no-sort no-click" id="bread-actions">
                    <a href="{{ url()->route('voyager.static_content.index') }}/{{ $row->id }}" title="View" class="btn btn-sm btn-warning pull-right view">
                        <i class=""></i> <span class="hidden-xs hidden-sm"> View</span>
                    </a>

                    <a href="{{ url()->route('voyager.static_content.index') }}/{{ $row->id }}/edit" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                        <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                    </a>

                    <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="{{ $row->id }}" id="delete-{{ $row->id }}">
                        <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>