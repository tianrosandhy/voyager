@extends('main::master')

<?php
if(!isset($type)){
    $type = $dataTypeContent->posttype;
    $by = 'id';
}
else{
    $by = 'slug';
}
?>

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif


                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp


                            <div class="form-group" id="post-type-holder">
                            	<label>Post Type</label>
                                @php
                                    $checkedPosttype = old('posttype', $dataTypeContent->posttype);
                                @endphp
                            	<select name="posttype" class="form-control">
                            		<option value=""></option>
                            		@foreach(StaticContent::getPosttype() as $row)
                            		<option value="{{ $row->id }}" {{ $checkedPosttype == $row->id ? 'selected' : '' }}>{{ $row->title }}</option>
                            		@endforeach
                            	</select>
                            </div>

                            <div class="form-group">
                                <label>Parameter Name</label>
                                <input type="text" name="param" class="form-control" value="{{ old('param', $dataTypeContent->param) }}">
                            </div>


                            <div class="form-group">
                            	<label>Type</label>
                                @php
                                    $checkedType = old('type', $dataTypeContent->type);
                                @endphp
                            	<select name="type" id="posttype-type" class="form-control">
                            		<option value="text" {{ $checkedType == 'text' ? 'selected' : '' }}>Text</option>
                            		<option value="textarea"  {{ $checkedType == 'textarea' ? 'selected' : '' }}>Textarea</option>
                            		<option value="rich"  {{ $checkedType == 'rich' ? 'selected' : '' }}>Rich Text Area</option>
                            		<option value="images"  {{ $checkedType == 'images' ? 'selected' : '' }}>Images</option>
                            		<option value="video"  {{ $checkedType == 'video' ? 'selected' : '' }}>Video URL</option>
                            	</select>
                            </div>


                            <div class="form-group">
                            	<label>Content</label>
                            	<div class="content-holder">
                                    @php
                                    if($checkedType){
                                        $content[$checkedType] = old('content.'.$checkedType, $dataTypeContent->content);
                                    }
                                    @endphp
                            		<div class="options" data-type="text">
                                        <input type="text" name="content[text]" class="form-control" value="{{ isset($content['text']) ? $content['text'] : '' }}">      
                                    </div>
                                    <div class="options" data-type="textarea">
                                        <textarea name="content[textarea]" class="form-control">{{ isset($content['textarea']) ? $content['textarea'] : '' }}</textarea>
                                    </div>
                                    <div class="options" data-type="rich">
                                        <textarea name="content[rich]" class="form-control richTextBox" id="richtextbody">{{ isset($content['rich']) ? $content['rich'] : '' }}</textarea>
                                    </div>
                                    <div class="options" data-type="images">
                                        @if(isset($content['images']))
                                        <div class="clearfix">
                                            <?php
                                            $dataImg = json_decode($content['images']);
                                            foreach($dataImg as $img){
                                                echo '<img src="'.Storage::url($img).'" height=100 align="left">';
                                            }
                                            ?>
                                        </div>
                                        @endif
                                        <input type="file" multiple="multiple" name="content[images][]" accept="image/*">
                                    </div>
                                    <div class="options" data-type="video">
                                        <input type="text" name="content[video]" class="form-control" placeholder="https://youtube.com/xxxx" value="{{ isset($content['video']) ? $content['video'] : '' }}">
                                    </div>

                            	</div>
                            </div>


                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            @if($type)
                <?php
                $instance = StaticContent::posttypeInstance($type, $by);
                ?>
                $("select[name='posttype']").val('{{ $instance->id }}');
                $("#post-type-holder").hide();
            @endif


            $('.toggleswitch').bootstrapToggle();

            $(".options").hide();
            showContentBasedOnType();
            $("#posttype-type").on('change', function(){
                showContentBasedOnType();
            });


            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });



        function showContentBasedOnType(){
            type = $("#posttype-type").val();
            $(".options[data-type!='"+type+"']").slideUp();
            $(".options[data-type='"+type+"']").slideDown();
        }
    </script>
@stop
