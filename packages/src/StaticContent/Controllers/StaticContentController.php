<?php
namespace Module\StaticContent\Controllers;

use Module\Main\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use Storage;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadDataDeleted;
use Intervention\Image\Facades\Image;
use Intervention\Image\Constraint;

use Module\StaticContent\Models\StaticContent;
use Module\StaticContent\Models\StaticPosttype;


class StaticContentController extends AdminBaseController
{

	public function __construct(Request $req){
		$this->request = $req;
		$this->hint = 'static_content';
	}

	public function index(Request $request, $posttype=''){
		// GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->hint;

        if(strlen($posttype) == 0){
	        $posttype = $this->request->posttype;
        }
        $filtered = self::checkPostType($posttype);
        if($filtered === false){
        	return redirect()->route('voyager.dashboard')->withErrors('Page not found');
        }

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();



        $view = 'main::browse';

        if (view()->exists($this->hint."::browse")) {
            $view = $this->hint."::browse";
        }

        $hint = $this->hint;
    	$viewData = compact(
    		'dataType',
    		'hint',
    		'filtered',
    		'posttype'
    	);


		return Voyager::view($view, $viewData);   
	}


	protected function checkPostType($posttype=''){
		$cek = StaticPosttype::where('slug', $posttype)->first();
		if(!empty($cek)){
			return self::getByPosttype($cek);
		}
		return false;
	}

	protected function getByPosttype($instance){
		return StaticContent::where('posttype', $instance->id)->get();
	}


	protected function getPosttypeSlug($id){
		$data = StaticPosttype::find($id)->first();
		return $data->slug;
	}





	public function create(Request $request, $type="")
    {
        $slug = $this->hint;

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
                            ? new $dataType->model_name()
                            : false;

        foreach ($dataType->addRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'main::edit-add';

        if (view()->exists($this->hint."::edit-add")) {
            $view = $this->hint."::edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'type'));
    }







	public function store(Request $request){
		$cek = self::validateRequest();
		//cek apakah konten sudah sesuai dengan type
		$post = $this->request->all();
		if(isset($post['content'][$this->request->type])){
			if(!empty($post['content'][$this->request->type])){
				//proses simpan static content

				$content = $post['content'][$this->request->type];
				if($post['type'] == 'images'){
					//save metode upload gambar
					$content = self::handleImageUpload($this->request->file('content')['images']);
				}

				StaticContent::create([
					'param' => $post['param'],
					'posttype' => $post['posttype'],
					'content' => $content,
					'type' => $post['type']
				]);

				return redirect(env('ADMIN_PREFIX').'/static_content/type/'.self::getPosttypeSlug($post['posttype']));
			}
		}

		return redirect()->back()->withInput()->withErrors('Please fill the content');
	}



	public function update(Request $request, $id)
    {
    	$cek = self::validateRequest();
    	$instance = StaticContent::find($id);
    	if(empty($instance)){
    		return back()->withErrors('Data not found');
    	}

    	//cek apakah konten sudah sesuai dengan type
		$post = $this->request->all();
		if(isset($post['content'][$this->request->type])){
			if(!empty($post['content'][$this->request->type])){
				//proses simpan static content

				$content = $post['content'][$this->request->type];
				if($post['type'] == 'images'){
					//save metode upload gambar
					$content = self::handleImageUpload($this->request->file('content')['images']);
				}

				$instance->update([
					'param' => $post['param'],
					'posttype' => $post['posttype'],
					'content' => $content,
					'type' => $post['type']
				]);

				return redirect(env('ADMIN_PREFIX').'/static_content/?posttype='.self::getPosttypeSlug($post['posttype']));
			}
		}

		return redirect()->back()->withInput()->withErrors('Please fill the content');

    }



    public function destroy(Request $request, $id)
    {
        $slug = $this->hint;

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        $posttype = '';
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
            $posttype = self::getPosttypeSlug($data->posttype);
            $this->cleanup($dataType, $data);
        }

        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->route("voyager.static_content.posttype", ['type' => $posttype])->with($data);
    }















	protected function validateRequest(){
		return \Validator::make($this->request->all(), [
			'posttype' => 'required',
			'param' => 'required',
			'type' => 'required'
		])->validate();
	}



}