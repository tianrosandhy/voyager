<?php

namespace Module\StaticContent;

use Module\Main\BaseServiceProvider;
use Module\StaticContent\Services\StaticContentService;

class StaticContentServiceProvider extends BaseServiceProvider
{
    public 
        $namespace = 'Module\StaticContent\Controllers',
        $hint = 'static_content',
        $dir = __DIR__;


    public function register()
    {
        //
        $this->app->router->group(['namespace' => $this->namespace, 'middleware' => ['web', 'admin.user']], function ($route) {
            require realpath(__DIR__.'/Routes/web.php');
        });

        $this->app->singleton('static_content-facade', function ($app) {
            return new StaticContentService($app);
        });

        $this->loadViewsFrom(realpath(__DIR__.'/Views/'), $this->hint);
    }
}
