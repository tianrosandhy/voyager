<?php

namespace Module\Event;

use Module\Main\BaseServiceProvider;

class EventServiceProvider extends BaseServiceProvider
{
    public 
        $namespace = 'Module\Event\Controllers'
        $hint = 'event',
        $dir = __DIR__;

}
